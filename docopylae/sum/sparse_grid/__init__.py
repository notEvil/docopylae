import docopylae.python_tools as d_python_tools
import docopylae.space as d_space
import os.path as o_path
import zipfile


# import docopylae.sum.space.base as dss_base
# import numpy


class SparseGridGetPoints:  # TODO refactor
    """
    - represents a callable which returns a sparse grid for docopylae.sum.transform.FixTransformer
    """

    def __init__(self, accuracy):  # (accuracy~int)~SparseGridGetPoints
        super().__init__()

        self.accuracy = accuracy

    def __call__(
        self, dimension_extents
    ):  # (dimension_extents~{dimension~d_space.Dimension: extent~dss_base.Extent})~(nodes~d_space.Points(coordinates~{dimension~d_space.Dimension: coordinates~[coordinate~object]}), weights~numpy.ndarray([weight~object]))
        nodes, weights = _get_sparse_grid(len(dimension_extents), self.accuracy)

        dimension_lengths = {
            dimension: extent.get_length()
            for dimension, extent in dimension_extents.items()
        }
        points = d_space.Points(
            {
                dimension: nodes[:, index]
                for index, dimension in enumerate(dimension_extents.keys())
            },
            length=len(nodes),
        ) * d_space.Point(dimension_lengths) + d_space.Point(
            {
                dimension: extent.begin_value
                for dimension, extent in dimension_extents.items()
            }
        )
        weights = weights * d_python_tools.get_product(dimension_lengths.values())

        result = (points, weights)
        return result


def _get_sparse_grid(n_dimensions, accuracy):
    import pandas

    path = o_path.join(o_path.dirname(__file__), "KPU.zip")
    with zipfile.ZipFile(path) as zip_file:
        with zip_file.open("KPU_d{}_l{}.asc".format(n_dimensions, accuracy)) as file:
            data_frame = pandas.read_csv(file, sep=",", header=None)

    result = (data_frame.iloc[:, :-1].values, data_frame.iloc[:, -1].values)
    return result
