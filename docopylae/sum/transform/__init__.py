import docopylae.python_tools as d_python_tools
import docopylae.space as d_space
import docopylae.sum as d_sum
import docopylae.sum.base as ds_base
import docopylae.sum.space as ds_space
import docopylae.sum.transform.base as dst_base
import numpy


# import docopylae.distribution.base as dd_base

# POINT = dd_base.POINT
# POINTS = dd_base.POINTS


class TransformedSum(ds_base.Sum):
    """
    - represents a transformed Sum
    """

    def __init__(
        self, sum, transformers
    ):  # (sum~Sum, transformers~[dst_base.Transformer])~TransformedSum
        super().__init__()

        self.sum = sum
        self.transformers = transformers

        self._sub_sums = None

    def get_sub_sums(self):  # ()~{d_sum.SubSum}
        if self._sub_sums is not None:
            return self._sub_sums

        sum = self.sum
        for transformer in self.transformers:
            sum = transformer.transform(sum)

        self._sub_sums = sum.get_sub_sums()
        return self._sub_sums


class ContinuousTransformer(dst_base.Transformer):
    """
    - represents an object which transforms Sum objects using continuous transformations
    - see dst_base.ContinuousTransformation
    """

    def __init__(
        self, continuous_transformations
    ):  # (continuous_transformations~[dst_base.ContinuousTransformation])~ContinuousTransformer
        super().__init__()

        self.continuous_transformations = continuous_transformations

    def transform(self, sum):  # (sum~d_sum.Sum)~d_sum.Sum
        sub_sums = [
            d_sum.SubSum(
                ContinuousTransformer._Function(sub_sum.function, self),
                ContinuousTransformer._Space(sub_sum.space, self),
            )
            for sub_sum in sum.get_sub_sums()
        ]
        result = d_sum.Sum(sub_sums)
        return result

    class _Function(ds_base.Function):
        def __init__(
            self, function, continuous_transformer
        ):  # (function~ds_base.Function, continuous_transformer~ContinuousTransformer)~ContinuousTransformer._Function
            super().__init__()

            self.function = function
            self.continuous_transformer = continuous_transformer

        def evaluate(self, inverse_point):  # (inverse_point~d_space.Point)~object
            point = None

            def _():
                nonlocal inverse_point, point

                for continuous_transformation in reversed(
                    self.continuous_transformer.continuous_transformations
                ):
                    point = continuous_transformation.transform_inverse_point(
                        inverse_point
                    )

                    jacobian = continuous_transformation.get_jacobian(
                        point, inverse_point
                    )
                    yield jacobian

                    inverse_point = point

            jacobian = d_python_tools.get_product(_())
            result = self.function.evaluate(point) * jacobian
            return result

        def evaluate_many(
            self, inverse_points
        ):  # (inverse_points~d_space.Points)~object
            points = None

            def _():
                nonlocal inverse_points, points

                for continuous_transformation in reversed(
                    self.continuous_transformer.continuous_transformations
                ):
                    points = continuous_transformation.transform_inverse_points(
                        inverse_points
                    )

                    jacobians = continuous_transformation.get_jacobians(
                        points, inverse_points
                    )
                    yield jacobians

                    inverse_points = points

            jacobians = d_python_tools.get_product(_())
            if False:
                print()
                print(points)
                print(self.function)
            if False:
                print()
                print(self.function.evaluate_many(points))
                print(jacobians)
            result = self.function.evaluate_many(points) * jacobians
            return result

    class _Space(ds_space.Space):
        def __init__(
            self, space, continuous_transformer
        ):  # (space~ds_space.Space, continuous_transformer~ContinuousTransformer)~ContinuousTransformer._Space
            super().__init__()

            self.space = space
            self.continuous_transformer = continuous_transformer

            self._sub_spaces = None

        def get_sub_spaces(self):  # ()~{ds_space.SubSpace}
            if self._sub_spaces is not None:
                return self._sub_spaces

            sub_spaces = self.space.get_sub_spaces()

            def _(sub_spaces, continuous_transformation):
                for sub_space in sub_spaces:
                    sub_space = self._transform_sub_space(
                        sub_space, continuous_transformation
                    )
                    yield sub_space

            for (
                continuous_transformation
            ) in self.continuous_transformer.continuous_transformations:
                sub_spaces = _(sub_spaces, continuous_transformation)

            self._sub_spaces = list(sub_spaces)
            return self._sub_spaces

        def _transform_sub_space(self, sub_space, continuous_transformation):
            begin_point = d_space.Point(
                {
                    dimension: extent.begin_value
                    if isinstance(extent, ds_space.IntervalExtent)
                    else None
                    for dimension, extent in sub_space.dimension_extents.items()
                }
            )
            end_point = d_space.Point(
                {
                    dimension: extent.end_value
                    if isinstance(extent, ds_space.IntervalExtent)
                    else None
                    for dimension, extent in sub_space.dimension_extents.items()
                }
            )
            begin_point = continuous_transformation.transform_point(begin_point)
            end_point = continuous_transformation.transform_point(end_point)

            begin_coordinates = begin_point.coordinates
            end_coordinates = end_point.coordinates

            extents = {
                dimension: ds_space.IntervalExtent(
                    begin_coordinates[dimension],
                    end_coordinates[dimension],
                )
                if isinstance(extent, ds_space.IntervalExtent)
                else extent
                for dimension, extent in sub_space.dimension_extents.items()
            }
            result = ds_space.SubSpace(extents)
            return result


class ProbabilityIntegralTransformation(dst_base.ContinuousTransformation):
    def __init__(
        self, dimension_arguments
    ):  # (dimension_arguments~{dimension~d_space.Dimension: (distribution~dd_base.ProbabilityDistribution, transformation point~POINT)})~ProbabilityIntegralTransformation
        super().__init__()

        self.dimension_arguments = dimension_arguments

    def transform_point(self, point):  # (point~d_space.Point)~d_space.Point
        coordinates = point.coordinates.copy()
        for (
            dimension,
            (distribution, transformation_point),
        ) in self.dimension_arguments.items():
            coordinates[dimension] = distribution.get_cumulative_probability(
                transformation_point & point
            )
        result = d_space.Point(coordinates)
        return result

    def transform_inverse_point(
        self, inverse_point
    ):  # (inverse_point~d_space.Point)~d_space.Point
        coordinates = inverse_point.coordinates.copy()
        for (
            dimension,
            (distribution, transformation_point),
        ) in self.dimension_arguments.items():
            coordinates[dimension] = distribution.get_quantile(
                coordinates[dimension], transformation_point
            )
        result = d_space.Point(coordinates)
        return result

    def transform_inverse_points(
        self, inverse_points
    ):  # (inverse_points~d_space.Points)~d_space.Points
        if False:
            print()
            print(inverse_points)
        result = inverse_points.copy()
        for (
            dimension,
            (distribution, transformation_point),
        ) in self.dimension_arguments.items():
            (result.coordinates[dimension],) = distribution.get_quantiles(
                result[dimension],
                transformation_point.get_points(len(inverse_points)),
            ).get_coordinates()
        if False:
            print(result)
        return result

    def get_jacobian(
        self, point, inverse_point
    ):  # (point~d_space.Point, inverse_point~d_space.Point)~object
        result = 1 / d_python_tools.get_product(
            distribution.get_probability(transformation_point & point)
            for dimension, (
                distribution,
                transformation_point,
            ) in self.dimension_arguments.items()
        )
        return result

    def get_jacobians(
        self, points, inverse_points
    ):  # (points~d_space.Points, inverse_points~d_space.Points)~numpy.ndarray([jacobian~object])
        if False:
            print()
            print(points)
            print(inverse_points)
            print(
                [
                    transformation_point.get_points(len(points)) & points
                    for _, transformation_point in self.dimension_arguments.values()
                ]
            )
        result = 1 / d_python_tools.get_product(
            distribution.get_probabilities(
                transformation_point.get_points(len(points)) & points
            )
            for dimension, (
                distribution,
                transformation_point,
            ) in self.dimension_arguments.items()
        )
        if False:
            print(result)
        return result


class FixTransformer(dst_base.Transformer):  # TODO find better name
    """
    - represents an object which transforms Sum objects to approximate integrals by sums
    - see SparseGridGetPoints
    """

    # GET_POINTS = function(dimension extents~{dimension~d_space.Dimension: interval extent~ds_space.IntervalExtent})~(nodes~d_space.Points(coordinates~{dimension~d_space.Dimension: coordinates~[coordinate~object]}), weights~numpy.ndarray([weight~object]))

    def __init__(self, get_points):  # (get_points~GET_POINTS)~FixTransformer
        super().__init__()

        self.get_points = get_points

    def transform(self, sum):  # (sum~d_sum.Sum)~d_sum.Sum
        sub_sums = [
            d_sum.SubSum(
                sub_sum.function,
                FixTransformer._Space(sub_sum.space, self),
            )
            for sub_sum in sum.get_sub_sums()
        ]
        result = d_sum.Sum(sub_sums)
        return result

    class _Space(ds_space.Space):
        def __init__(
            self, space, fix_transformer
        ):  # (space~ds_space.Space, fix_transformer~FixTransformer)~FixSumTransformer._Space
            super().__init__()

            self.space = space
            self.fix_transformer = fix_transformer

            self._sub_spaces = None

        def get_sub_spaces(self):  # ()~{ds_space.SubSpace}
            if self._sub_spaces is not None:
                return self._sub_spaces

            sub_spaces = []

            for sub_space in self.space.get_sub_spaces():
                interval_extent_by_dimension = {
                    dimension: extent
                    for dimension, extent in sub_space.dimension_extents.items()
                    if isinstance(extent, ds_space.IntervalExtent)
                }

                if len(interval_extent_by_dimension) == 0:
                    sub_spaces.append(sub_space)
                    continue

                points, weights = self.fix_transformer.get_points(
                    interval_extent_by_dimension
                )

                dimension_extents = sub_space.dimension_extents.copy()
                scatter_group = ds_space.ScatterGroup(weights=weights)
                dimension_extents.update(
                    (
                        dimension,
                        ds_space.ScatterExtent(points[dimension], scatter_group),
                    )
                    for dimension in interval_extent_by_dimension.keys()
                )

                sub_spaces.append(ds_space.SubSpace(dimension_extents))

            self._sub_spaces = sub_spaces
            return self._sub_spaces
