# import docopylae.sum as d_sum
# import docopylae.space as d_space


class Transformer:
    """
    - represents an object which transforms Sum objects into Sum objects
    """

    def transform(self, sum):  # (sum~d_sum.Sum)~d_sum.Sum
        raise NotImplementedError(type(self))


class ContinuousTransformation:
    """
    - represents a continuous transformation of some integral (integration by substitution)
    """

    def transform_point(self, point):  # (point~d_space.Point)~d_space.Point
        raise NotImplementedError(type(self))

    def transform_inverse_point(
        self, inverse_point
    ):  # (inverse_point~d_space.Point)~d_space.Point
        raise NotImplementedError(type(self))

    def get_jacobian(
        self, point, inverse_point
    ):  # (point~d_space.Point, inverse_point~d_space.Point)~d_space.Point
        raise NotImplementedError(type(self))

    def get_jacobians(
        self, points, inverse_points
    ):  # (points~d_space.Points, inverse_points~d_space.Points)~d_space.Points
        raise NotImplementedError(type(self))
