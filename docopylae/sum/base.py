import docopylae.python_tools as d_python_tools


# import docopylae.space as d_space


class Function:
    def evaluate(self, point):  # (point~d_space.Point)~object
        raise NotImplementedError(type(self))

    def evaluate_many(self, points):  # (points~d_space.Points)~[value~object]
        raise NotImplementedError(type(self))


class Sum:
    """
    - represents a nested sum or integral
    - see docopylae.sum.SubSum
    """

    def get_sub_sums(self):  # ()~{sub sum~SubSum}
        raise NotImplementedError(type(self))

    def evaluate(self):  # ()~object
        result = d_python_tools.get_sum(
            sub_sum.evaluate() for sub_sum in self.get_sub_sums()
        )
        return result
