import docopylae.space as d_space
import docopylae.space.base as ds_base
import docopylae.sum.space.base as dss_base
import numpy


# import docopylae.sum.space as ds_space


class Space(ds_base.Space):
    """
    - represents a fixed space for Sum
    """

    def get_sub_spaces(self):  # ()~{SubSpace}
        raise NotImplementedError(type(self))

    def get_points(self):  # ()~d_space.Points
        result = d_space.Points.stack(
            sub_space.get_points() for sub_space in self.get_sub_spaces()
        )
        return result


class SubSpace:
    """
    - represents a part of Space
    """

    def __init__(
        self, dimension_extents
    ):  # (dimension_extents~{dimension~d_space.Dimension: extent~dss_base.Extent})~SubSpace
        super().__init__()

        self.dimension_extents = dimension_extents

    def get_points(self):  # ()~d_space.Points
        result, _ = self.get_points_weights()
        return result

    def get_points_weights(self):  # ()~(d_space.Points, numpy.ndarray([weight~object]))
        parts = []
        scatter_coordinates = {}

        for dimension, extent in self.dimension_extents.items():
            if isinstance(extent, ScatterExtent):
                scatter_coordinates.setdefault(extent.scatter_group, {})[
                    dimension
                ] = numpy.asarray(
                    extent.coordinates
                )  # remember
                continue

            if isinstance(extent, GridExtent):
                parts.append(
                    (
                        d_space.Points({dimension: numpy.asarray(extent.coordinates)}),
                        extent.weights,
                    )
                )
                continue

            raise TypeError(type(extent))

        if len(scatter_coordinates) != 0:
            parts.extend(
                (d_space.Points(coordinates), scatter_group.weights)
                for scatter_group, coordinates in scatter_coordinates.items()
            )

        if len(parts) == 0:
            result = (d_space.Points({}, length=0), numpy.array([]))
            return result

        parts = iter(parts)
        result_points, result_weights = next(parts)

        for points, weights in parts:
            if weights is not None:
                # WARNING depends on internals of d_space.Points.get_cartesian_product
                weights = numpy.tile(weights, len(result_points))
                result_weights = (
                    weights
                    if result_weights is None
                    else (numpy.repeat(result_weights, len(points)) * weights)
                )

            result_points = result_points.get_cartesian_product(points)

        result = (result_points, result_weights)
        return result


class ScatterExtent(dss_base.Extent):
    """
    - represents dependent coordinates for some d_space.Dimension and ScatterGroup
    """

    def __init__(
        self, coordinates, scatter_group
    ):  # (coordinates~[coordinate~object], scatter_group~ScatterGroup)~ScatterExtent
        super().__init__()

        self.coordinates = coordinates
        self.scatter_group = scatter_group

    def __len__(self):
        result = len(self.coordinates)
        return result


class ScatterGroup:
    """
    - represents a group of ScatterExtent
    """

    def __init__(self, weights=None):  # (weights~[weight~object] or None)
        self.weights = weights


class GridExtent(dss_base.Extent):
    """
    - represents independent coordinates for some d_space.Dimension
    """

    def __init__(
        self, coordinates, weights=None
    ):  # (coordinates~[coordinate~object], weights~[weight~object] or None)~GridExtent
        super().__init__()

        self.coordinates = coordinates
        self.weights = weights

    def __len__(self):
        result = len(self.coordinates)
        return result


class IntervalExtent(dss_base.Extent):
    """
    - represents a closed interval for some d_space.Dimension
    """

    def __init__(
        self, begin_value, end_value
    ):  # (begin_value~object, end_value~object)~IntervalExtent
        super().__init__()

        self.begin_value = begin_value
        self.end_value = end_value

    def get_length(self):  # ()~object
        result = self.end_value - self.begin_value
        return result


class FunctionExtent(dss_base.Extent):
    """
    - represents a functionally dependent extent for some d_space.Dimension
    """

    # GET_EXTENT = function(point~d_space.Point)~dss_base.Extent

    def __init__(self, get_extent):  # (get_extent~GET_EXTENT)~FunctionExtent
        super().__init__()

        self.get_extent = get_extent
