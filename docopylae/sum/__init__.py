import docopylae.python_tools as d_python_tools
import docopylae.sum.base as ds_base
import docopylae.sum.space as ds_space


# import docopylae.sum.base as ds_base


class Sum(ds_base.Sum):
    def __init__(self, sub_sums):
        super().__init__()

        self.sub_sums = sub_sums

    def get_sub_sums(self):  # ()~{sub sum~SubSum}
        return self.sub_sums


class SubSum:
    """
    - represents a part of Sum
    """

    def __init__(
        self, function, space
    ):  # (function~ds_base.Function, space~ds_space.Space)~SubSum
        super().__init__()

        self.function = function
        self.space = space

    def evaluate(self):  # ()~object
        def _():
            for sub_space in self.space.get_sub_spaces():
                if all(
                    isinstance(extent, (ds_space.GridExtent, ds_space.ScatterExtent))
                    for extent in sub_space.dimension_extents.values()
                ):
                    points, weights = sub_space.get_points_weights()

                    function_result = self.function.evaluate_many(points)
                    if weights is not None:
                        function_result = function_result * weights

                    sub_result = d_python_tools.get_sum(function_result)
                    yield sub_result
                    continue

                raise NotImplementedError(type(self))

        result = d_python_tools.get_sum(_())
        return result
