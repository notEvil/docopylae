import docopylae.distribution as d_distribution
import docopylae.jax.distribution.base as djd_base
import docopylae.space as d_space
import jax
import jax.numpy as j_numpy
import jax.scipy.stats as js_stats


# POINT = djd_base.POINT
# POINTS = djd_base.POINTS


def initialize():  # ()~None
    import docopylae.jax.distribution.copula as djd_copula

    d_distribution.UnivariateUniformDistribution.set_specialization_type(
        jax, UnivariateUniformDistribution
    )
    d_distribution.UnivariateNormalDistribution.set_specialization_type(
        jax, UnivariateNormalDistribution
    )

    djd_copula.initialize()


class UnivariateUniformDistribution(djd_base.ProbabilityDistribution):

    # probabilities

    def get_probabilities(
        self, points
    ):  # (points~POINTS)~j_numpy.ndarray([probability~number])
        result = j_numpy.ones((len(points),))
        return result


class UnivariateNormalDistribution(djd_base.ProbabilityDistribution):

    # probabilities

    def get_probability(self, point):  # (point~POINT)~number
        result = self._get_probability(point)
        return result

    def get_probabilities(
        self, points
    ):  # (points~POINTS)~j_numpy.ndarray([probability~number])
        result = self._get_probability(points)
        return result

    def _get_probability(self, point):
        (random_variable,) = self.get_random_variables()
        outcome = point[random_variable]
        (mean,) = self.get_mean(point).coordinates.values()
        (standard_deviation,) = self.get_standard_deviation(point).coordinates.values()
        result = (
            js_stats.norm.pdf((outcome - mean) / standard_deviation)
            / standard_deviation
        )
        return result

    def get_cumulative_probability(self, point):  # (point~POINT)~number
        (random_variable,) = self.get_random_variables()
        outcome = point[random_variable]
        (mean,) = self.get_mean(point).coordinates.values()
        (standard_deviation,) = self.get_standard_deviation(point).coordinates.values()
        result = js_stats.norm.cdf((outcome - mean) / standard_deviation)
        return result

    def get_cumulative_probabilities(
        self, points
    ):  # (points~POINTS)~j_numpy.ndarray([cumulative probability~number])
        (random_variable,) = self.get_random_variables()
        outcomes = points[random_variable]
        (means,) = self.get_means(points).get_coordinates()
        (standard_deviations,) = self.get_standard_deviations(points).get_coordinates()
        result = js_stats.norm.cdf((outcomes - means) / standard_deviations)
        return result

    def get_quantiles(
        self, cumulative_probabilities, points
    ):  # (cumulative_probabilities~j_numpy.ndarray([cumulative probability~number]), points~POINTS)~POINTS
        (random_variable,) = self.get_random_variables()
        (means,) = self.get_means(points).get_coordinates()
        (standard_deviation,) = self.get_standard_deviations(points).get_coordinates()
        result = d_space.Points(
            {
                random_variable: js_stats.norm.ppf(cumulative_probabilities)
                * standard_deviations
                + means
            },
            length=len(points),
        )
        return result
