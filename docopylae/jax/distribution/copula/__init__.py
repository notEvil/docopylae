import docopylae.distribution.copula as dd_copula
import docopylae.jax.distribution.base as djd_base
import jax
import jax.numpy as j_numpy


# POINT = djd_base.POINT
# POINTS = djd_base.POINTS


def initialize():  # ()~None
    import docopylae.jax.distribution.copula.base as djdc_base
    import docopylae.jax.distribution.copula.vine as djdc_vine

    dd_copula.ProductCopulaDistribution.set_specialization_type(
        jax, ProductCopulaDistribution
    )

    djdc_base.initialize()
    djdc_vine.initialize()


class ProductCopulaDistribution(djd_base.ProbabilityDistribution):

    # probabilities

    def get_probabilities(
        self, points
    ):  # (points~POINTS)~j_numpy.ndarray([probability~number])
        result = j_numpy.ones((len(points),))
        return result
