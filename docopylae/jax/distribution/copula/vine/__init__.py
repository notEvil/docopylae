import docopylae.distribution.copula.vine as ddc_vine
import docopylae.jax.distribution.base as djd_base
import jax


# POINT = djd_base.POINT
# POINTS = djd_base.POINTS


def initialize():  # ()~None
    ddc_vine.VineCopulaDistribution.set_specialization_type(jax, VineCopulaDistribution)
    ddc_vine._VineMarginalDistribution.set_specialization_type(
        jax, _VineMarginalDistribution
    )


class VineCopulaDistribution(djd_base.ProbabilityDistribution):

    # probabilities

    def get_probabilities(
        self, points
    ):  # (points~POINTS)~j_numpy.ndarray([probability~number])
        result = self._get_probabilities(jax, points)
        return result


class _VineMarginalDistribution(djd_base.ProbabilityDistribution):

    # probabilities

    def get_cumulative_probabilities(
        self, points
    ):  # (points~POINTS)~j_numpy.ndarray([cumulative probability~number])
        result = self._get_cumulative_probabilities(jax, points)
        return result

    def get_quantiles(
        self, cumulative_probabilities, points
    ):  # (cumulative_probabilities~j_numpy.ndarray([cumulative probability~number]), points~POINTS)~POINTS
        result = self._get_quantiles(jax, cumulative_probabilities, points)
        return result
