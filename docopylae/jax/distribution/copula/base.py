import docopylae.distribution.copula.base as ddc_base
import docopylae.jax.distribution.base as djd_base
import jax


# import jax.numpy as j_numpy

# POINT = djd_base.POINT
# POINTS = djd_base.POINTS


def initialize():  # ()~None
    ddc_base.MetaDistribution.set_specialization_type(jax, MetaDistribution)


class MetaDistribution(djd_base.ProbabilityDistribution):

    # probabilities

    def get_probability(self, point):  # (point~POINT)~number
        result = self.specializable._get_probability(jax, point)
        return result

    def get_probabilities(
        self, points
    ):  # (points~POINTS)~j_numpy.ndarray([probability~number])
        result = self.specializable._get_probabilities(jax, points)
        return result
