import docopylae.distribution.base as dd_base
import docopylae.python_tools as d_python_tools


# import docopylae.distribution as d_distribution
# import jax.numpy as j_numpy

# POINT = dd_base.POINT
# POINTS = dd_base.POINTS


class ProbabilityDistribution(
    dd_base.CompositeDistribution, d_python_tools.Specialization
):
    def get_base_distribution(self):  # ()~dd_base.ProbabilityDistribution
        return self.specializable

    # probabilities

    # get_probabilities(points~POINTS)~j_numpy.ndarray([probability~number])

    # get_cumulative_probabilities(points~POINTS)~j_numpy.ndarray([cumulative probability~number])

    # get_quantiles(cumulative_probabilities~j_numpy.ndarray([cumulative probability~number]), points~POINTS)~POINTS

    # gradients

    # get_cumulative_probability_gradients(points~POINTS, random_variable~d_distribution.RandomVariable)~j_numpy.ndarray([gradient~number])

    # get_cumulative_probability_gradient_inverses(gradients~j_numpy.ndarray([gradient~number]), points~POINTS, random_variable~d_distribution.RandomVariable)~POINTS
