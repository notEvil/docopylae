import docopylae.base as d_base
import docopylae.batch as d_batch
import docopylae.space as d_space
import docopylae.space.batch as ds_batch
import jax
import jax.numpy as j_numpy
import math


# import docopylae.distribution as d_distribution
# import docopylae.distribution.base as dd_base
# import docopylae.jax.distribution.base as djd_base

# POINT = djd_base.POINT
# POINTS = djd_base.POINTS


def initialize():  # ()~None
    import docopylae.jax.distribution as dj_distribution
    import docopylae.jax.space as dj_space

    jax.config.update("jax_enable_x64", True)
    jax.config.update("jax_numpy_rank_promotion", "warn")

    dj_distribution.initialize()
    dj_space.initialize()


class BatchSizeFunction:
    def __init__(
        self, max_size, max_divisions=10, max_loss=0.5
    ):  # (max_size~int, max_divisions~int, max_loss~float)~BatchSizeFunction
        self.max_size = max_size
        self.max_divisions = max_divisions
        self.max_loss = max_loss

        self._factor = 1 / (max_divisions * (max_loss + 1))

    def __call__(self, total_size):
        lower_bound = self.max_size / (self.max_loss + 1)

        while total_size < lower_bound:
            lower_bound *= self._factor

        result = int(math.ceil(lower_bound * (self.max_loss + 1)))
        if result == 0:
            result = 1

        return result


class SecondDerivative(d_base.SecondDerivative):
    def __init__(
        self,
        parameters,
        distribution,
        jit=False,
        get_batch_size=BatchSizeFunction(int(1e3)),
    ):  # (parameters~{d_distribution.Parameter}, distribution~dd_base.ProbabilityDistribution, jit~bool, get_batch_size~function(total size~int)~int  or None)~SecondDerivative
        super().__init__()

        self.parameters = parameters
        self.distribution = distribution
        self.jit = jit
        self.get_batch_size = get_batch_size

        self._get_hessian = None
        self._get_hessians = None

    def evaluate(point):  # (point~POINT)~d_space.Matrix
        point, residual_point = self._split_point(point)
        result = self._get_get_hessian()(
            _get_float_point(point),
            residual_point,
        )
        return result

    def evaluate_many(self, points):  # (points~POINTS)~d_space.Matrices
        points, residual_points = self._split_points(points)
        get_hessians = self._get_get_hessians()

        if self.get_batch_size is not None:
            get_hessians = d_batch.BatchedFunction(
                get_hessians,
                self.get_batch_size(len(points)),
                ds_batch.PointsBatchInterface,
                ds_batch.MatricesBatchInterface,
            )

        result = self._get_get_hessians()(_get_float_points(points), residual_points)
        return result

    def _split_point(self, point):
        coordinates = point.coordinates.copy()
        point = d_space.Point(
            {parameter: coordinates.pop(parameter) for parameter in self.parameters}
        )
        residual_point = d_space.Point(coordinates)
        result = (point, residual_point)
        return result

    def _split_points(self, points):
        coordinates = points.coordinates.copy()
        points = d_space.Points(
            {parameter: coordinates.pop(parameter) for parameter in self.parameters},
            length=len(points),
        )
        residual_points = d_space.Points(coordinates, length=len(points))
        result = (points, residual_points)
        return result

    def _get_get_hessian(self):
        if self._get_hessian is not None:
            return self._get_hessian

        jax_function = jax.hessian(self._get_log_likelihood, argnums=1)

        if self.jit:
            jax_function = jax.jit(jax_function)

        def result(point, residual_point):
            result = jax_function(point, residual_point)
            result = d_space.get_matrix(
                {
                    parameter: point.coordinates
                    for parameter, point in result.coordinates.items()
                }
            )
            return result

        self._get_hessian = result
        return result

    def _get_get_hessians(self):
        if self._get_hessians is not None:
            return self._get_hessians

        if False:
            jax_function = jax.hessian(self._get_log_likelihoods, argnums=0)
        if False:
            jax_function = jax.jacfwd(
                jax.jacfwd(self._get_log_likelihoods, argnums=0), argnums=0
            )  # TODO replace by jax.hessian
        if True:
            jax_function = jax.vmap(
                jax.hessian(self._get_log_likelihood, argnums=0)
            )  # TODO might not work

        if self.jit:
            jax_function = jax.jit(jax_function)

        def result(points, residual_points):
            result = jax_function(
                d_space.Point(points.get_dictionary()),
                d_space.Point(residual_points.get_dictionary()),
            )
            result = d_space.get_matrices(
                {
                    parameter: point.coordinates
                    for parameter, point in result.coordinates.items()
                }
            )
            return result

        self._get_hessians = result
        return result

    def _get_log_likelihood(self, point, residual_point):
        point = point & residual_point
        result = j_numpy.log(
            self.distribution.get_specialization(jax).get_probability(point)
        )
        return result

    def _get_log_likelihoods(self, points, residual_points):
        points = points & residual_points
        result = j_numpy.log(
            self.distribution.get_specialization(jax).get_probabilities(points)
        )
        return result


def _get_float_point(point):
    result = d_space.Point(
        {
            dimension: float(coordinate)
            for dimension, coordinate in point.coordinates.items()
        }
    )
    return result


def _get_float_points(points):
    result = d_space.Points(
        {
            dimension: coordinates.astype(float)
            for dimension, coordinates in points.coordinates.items()
        },
        length=len(points),
    )
    return result
