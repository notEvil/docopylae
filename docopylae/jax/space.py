import docopylae.space as d_space
import jax.core as j_core
import jax.tree_util as j_tree_util


def initialize():  # ()~None
    j_tree_util.register_pytree_node(d_space.Point, _flatten_point, _unflatten_point)
    j_tree_util.register_pytree_node(
        d_space.Points,
        _flatten_points,
        _unflatten_points,
    )


def _flatten_point(point):
    result = ((point.coordinates,), None)
    return result


def _unflatten_point(_, children):
    result = d_space.Point(*children)
    # - jax nested diff will return nested Point
    # - jax vmap will return Point
    return result


def _flatten_points(points):
    if False:  # TODO remove
        print("flatten")
        print(repr(points))
        _ = next(iter(points.coordinates.values()))
        if type(_) is object or _ is None or isinstance(_, j_core.Tracer):
            result = ((points.coordinates,), None)
            return result

    result = ((points.get_dictionary(),), None)
    return result


def _unflatten_points(_, children):
    (coordinates,) = children

    if False:  # TODO remove
        print("unflatten")
        print(repr(coordinates))

        _ = next(iter(coordinates.values()))
        if type(_) is object or _ is None:
            result = d_space.Points(coordinates, length=0)
            return result

        if isinstance(_, j_core.Tracer):
            result = d_space.Points(
                coordinates, length=0 if len(_.shape) == 0 else _.shape[0]
            )
            return result

    result = d_space.Points(coordinates)
    return result


def _is_object(coordinates):
    result = len(coordinates) != 0 and type(next(iter(coordinates.values()))) is object
    return result
