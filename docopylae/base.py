import docopylae.python_tools as d_python_tools


# import docopylae.distribution as d_distribution
# import docopylae.distribution.base as dd_base
# import docopylae.space as d_space

# POINT = dd_base.POINT
# POINTS = dd_base.POINTS


class SecondDerivative:  # TODO better name
    """
    - represents a parameterized second derivative of the log likelihood with respect to the parameters
    """

    def evaluate(point):  # (point~POINT)~d_space.Matrix
        raise NotImplementedError(type(self))

    def evaluate_many(self, points):  # (points~POINTS)~d_space.Matrices
        raise NotImplementedError(type(self))


class FisherInformation:
    """
    - represents a parameterized Fisher information matrix
    """

    def evaluate(self, point):  # (point~POINT)~d_space.Matrix
        raise NotImplementedError(type(self))

    def evaluate_many(self, points):  # (points~POINTS)~d_space.Matrices
        raise NotImplementedError(type(self))


class Design:
    """
    - represents a fixed design
    """

    pass


class ApproximateDesign(Design):
    # POINT_WEIGHTS = {control point~POINT: weight~object}

    def get_point_weights(
        self,
    ):  # iter(point weight~(control point~POINT, weight~object))
        raise NotImplementedError(type(self))


class Criterion(d_python_tools.Specializable):
    def evaluate(self, design):  # (design~Design)~object
        raise NotImplementedError(type(self))

    def get_sensitivity(self):  # ()~Sensitivity
        raise NotImplementedError(type(self))


class CompositeCriterion(Criterion):
    def get_base_criterion(self):  # ()~Criterion
        raise NotImplementedError(type(self))

    def evaluate(self, design):  # (design~Design)~object
        criterion = self.get_base_criterion()
        result = criterion.evaluate(design)
        return result

    def get_sensitivity(self):  # ()~Sensitivity
        criterion = self.get_base_criterion()
        result = criterion.get_sensitivity()
        return result


class Sensitivity(d_python_tools.Specializable):
    """
    - represents a parameterized sensitivity
    """

    def evaluate(
        self, control_point, design
    ):  # (control_point~POINT, design~Design)~number
        raise NotImplementedError(type(self))

    def evaluate_many(
        self, control_points, design
    ):  # (control_points~POINTS, design~Design)~[sensitivity~number]
        raise NotImplementedError(type(self))


class CompositeSensitivity(Sensitivity):
    def get_base_sensitivity(self):  # ()~Sensitivity
        raise NotImplementedError(type(self))

    def evaluate(
        self, control_point, design
    ):  # (control_point~POINT, design~Design)~number
        sensitivity = self.get_base_sensitivity()
        result = sensitivity.evaluate(control_point, design)
        return result

    def evaluate_many(
        self, control_points, design
    ):  # (control_points~POINTS, design~Design)~[sensitivity~number]
        sensitivity = self.get_base_sensitivity()
        result = sensitivity.evaluate_many(control_points, design)
        return result


class DesignOptimizer:
    """
    - represents an object which finds an optimal design
    """

    def optimize(self, design=None):  # (design~Design or None)~Design
        raise NotImplementedError(type(self))
