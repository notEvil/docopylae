import docopylae.batch as d_batch
import docopylae.batch.base as db_base
import docopylae.space as d_space
import numpy


class PointsBatchInterface(db_base.BatchInterface):
    @staticmethod
    def get_length(object):  # (object~d_space.Points)~int
        result = len(object)
        return result

    @staticmethod
    def get_batch(slice, object):  # (slice~slice, object~d_space.Points)~d_space.Points
        result = object[slice]
        return result

    @staticmethod
    def get_fill(length, object):  # (length~int, object~d_space.Points)~d_space.Points
        result = d_space.Points(
            d_batch.ArrayDictBatchInterface.get_fill(length, object.get_dictionary()),
            length=length,
        )
        return result

    @staticmethod
    def concatenate_batches(objects):  # (objects~iter(d_space.Points))~d_space.Points
        result = d_space.Points.stack(objects)
        return result


class MatricesBatchInterface(db_base.BatchInterface):
    @staticmethod
    def get_length(object):  # (object~d_space.Matrices)~int
        result = len(object)
        return result

    @staticmethod
    def get_batch(
        slice, object
    ):  # (slice~slice, object~d_space.Matrices)~d_space.Matrices
        result = object[slice]
        return result

    @staticmethod
    def get_fill(
        length, object
    ):  # (length~int, object~d_space.Matrices)~d_space.Matrices
        coordinates = numpy.full_like(
            object.coordinates,
            numpy.nan,
            shape=(length,) + object.coordinates.shape[1:],
        )
        result = d_space.Matrices(
            coordinates, object.get_row_dimensions(), object.get_column_dimensions()
        )
        return result

    @staticmethod
    def concatenate_batches(
        objects,
    ):  # (objects~iter(d_space.Matrices))~d_space.Matrices
        objects = iter(objects)
        first_object = next(objects)

        row_dimensions = first_object.get_row_dimensions()
        column_dimensions = first_object.get_column_dimensions()

        _ = [first_object.coordinates]
        _.extend(
            object[row_dimensions, column_dimensions].coordinates for object in objects
        )
        coordinates = numpy.concatenate(_, axis=0)

        result = d_space.Matrices(coordinates, row_dimensions, column_dimensions)
        return result
