# TODO make matmul position consistent


import docopylae.batch as d_batch
import docopylae.python_tools as d_python_tools
import numpy
import collections.abc as c_abc
import operator


class Dimension:
    """
    - represents a dimension
    """

    def __init__(self, id):  # (id~object)~Dimension
        super().__init__()

        self.id = id

        self._hash = None

    def __hash__(self):
        if self._hash is not None:
            return self._hash

        self._hash = hash((Dimension, self.id))
        return self._hash

    def __eq__(self, object):
        result = isinstance(object, Dimension) and object.id == self.id
        return result

    def __lt__(self, object):
        if not isinstance(object, Dimension):
            raise TypeError(type(object))

        result = self.id < object.id
        return result


class _Dimensions(c_abc.Sequence):
    def __init__(self, dimensions):
        self.dimensions = dimensions

        self._indices = {dimension: index for index, dimension in enumerate(dimensions)}

    def get_index(self, dimension):
        result = self._indices[dimension]
        return result

    def __iter__(self):
        result = iter(self.dimensions)
        return result

    def __getitem__(self, index):
        result = self.dimensions[index]
        return result

    def __len__(self):
        result = len(self.dimensions)
        return result

    def __eq__(self, object):
        result = (
            isinstance(object, _Dimensions) and object.dimensions == self.dimensions
        )
        return result

    def is_equivalent_to(self, sequence):
        if sequence is self:
            return True

        if isinstance(sequence, _Dimensions):
            result = sequence.dimensions == self.dimensions
            return result

        result = sequence == self.dimensions
        return result


class Point:
    """
    - like {dimension~Dimension: coordinate~object}
    - hashable
    - [Dimension]
      - returns object
    - [{Dimension}]
      - returns Point
    - +-*/
    - union
      - & Point
    """

    def __init__(self, coordinates, transposed=False):  # ({dimension~Dimension: coordinate~object}, transposed~bool)~Point
        # TODO carry transposed forward everywhere
        super().__init__()

        self.coordinates = coordinates
        self.transposed = transposed

        self._hash = None

    def __getitem__(self, key):  # (key~Dimension)~object  or  (key~{Dimension})~Point
        if isinstance(key, Dimension):
            result = self.coordinates[key]
            return result

        result = Point({dimension: self.coordinates[dimension] for dimension in key})
        return result

    def __hash__(self):
        if self._hash is not None:
            return self._hash

        self._hash = hash(
            (Point, frozenset(self.coordinates.items()), self.transposed)
        )  # WARNING assumes coordinates are immutable
        return self._hash

    def __eq__(self, object):
        result = self._apply(operator.eq, object)
        return result

    def __bool__(self):
        result = all(coordinate is True for coordinate in self.coordinates.values())
        return result

    def __neg__(self):  # ()~Point
        result = Point(
            {
                dimension: -coordinates
                for dimension, coordinates in self.coordinates.items()
            }
        )
        return result

    def __add__(self, object):  # (object~object)~Point
        result = self._apply(operator.add, object)
        return result

    def __radd__(self, object):  # (object~object)~Point
        result = self + object
        return result

    def __sub__(self, object):  # (object~object)~Point
        result = self._apply(operator.sub, object)
        return result
        
    def __rsub__(self, object):  # (object~object)~Point
      result = -self + object
      return result

    def __mul__(self, object):  # (object~object)~Point
        result = self._apply(operator.mul, object)
        return result

    def __rmul__(self, object):  # (object~object)~Point
        result = self * object
        return result

    def __truediv__(self, object):  # (object~object)~Point
        result = self._apply(operator.truediv, object)
        return result

    def _apply(self, function, object):
        if isinstance(object, (Point, Points)):
            assert object.transposed == self.transposed
            
            if isinstance(object, Point):
                type_ = Point
                kwargs = {}

            else:
                type_ = Points
                kwargs = dict(length=len(object))

            result = type_(
                {
                    dimension: function(coordinates, object.coordinates[dimension])
                    for dimension, coordinates in self.coordinates.items()
                },
                **kwargs
            )
            return result

        result = Point(
            {
                dimension: function(coordinates, object)
                for dimension, coordinates in self.coordinates.items()
            }
        )
        return result

    def __and__(self, object):  # (object~Point)~Point
        if isinstance(object, Point):
            coordinates = self.coordinates.copy()
            coordinates.update(object.coordinates)
            result = Point(coordinates)
            return result

        raise TypeError(type(object))

    def __lt__(self, object):  # (object~Point)~bool
        if isinstance(object, Point):
            object_coordinates = object.coordinates
            result = all(
                coordinate < object_coordinates[dimension]
                for dimension, coordinate in self.coordinates.items()
            )
            return result

        raise TypeError(type(object))
    
    def __matmul__(self, object):  # TODO
        if isinstance(object, Point):
          dimensions = list(self.coordinates.keys())
          
          result = self.get_array(dimensions) @ object.get_array(dimensions)
          if len(result.shape) != 0:
            result = Matrix(result, dimensions, dimensions)
            
          return result
          
        result = object @ self
        return result
      
    def get_array(self, dimensions):  # (dimensions~[Dimension])~numpy.ndarray([coordinate~object])
      result = numpy.array([self.coordinates[dimension] for dimension in dimensions])
      if self.transposed:
        result = numpy.transpose(result)
      return result

    def __repr__(self):
        result = "Point({})".format(repr(self.coordinates))
        return result

    def get_points(self, length):  # (length~int)~Points
        result = Points(
            {
                dimension: numpy.array([coordinate])
                for dimension, coordinate in self.coordinates.items()
            },
            length=length,
        )
        return result
        
    def get_transposed(self):  # ()~Point
      result = Point(self.coordinates, transposed=not self.transposed)
      return result


class Points:
    """
    - like {dimension~Dimension: coordinates~numpy.ndarray([coordinate~object])}
    - iterable
      - yields Point
    - [Dimension]
      - returns [coordinate~object]
    - [{Dimension}]
      - returns Points
    - [int]
      - returns Point
    - [else]
      - returns Points
    - +-*/[=]
    - union
      - & Points
    - catesian product
    """

    def __init__(
        self, coordinates, length=None, transposed=False
    ):  # (coordinates~{dimension~Dimension: coordinates~numpy.ndarray([coordinate~object])}, length~int or None, transposed~bool)~Points
        # TODO carry transposed forward everywhere
        super().__init__()

        self.coordinates = coordinates
        self.length = length
        self.transposed = transposed

        if length is None:
            for coordinates in coordinates.values():
                self._length = len(coordinates)

                if self._length != 1:
                    break

        else:
            self._length = length

    def __len__(self):
        return self._length

    def __iter__(self):  # ()~iter(Point)
        dimensions = list(self.coordinates.keys())
        result = (
            Point(dict(zip(dimensions, coordinates)))
            for coordinates in zip(
                *[
                    self._get_broadcasted(self.coordinates[dimension])
                    for dimension in dimensions
                ]
            )
        )
        return result

    def __getitem__(
        self, key
    ):  # (key~Dimension)~numpy.ndarray([coordinate~object])  or  (key~{Dimension})~Points  or  (key~int)~Point  or  (key~object)~Points
        if isinstance(key, Dimension):
            result = self._get_broadcasted(self.coordinates[key])
            return result

        if isinstance(key, _Dimensions) or (
            isinstance(key, c_abc.Collection)
            and all(isinstance(item, Dimension) for item in key)
        ):
            result = Points(
                {dimension: self.coordinates[dimension] for dimension in key},
                length=self._length,
            )
            return result

        result = (Point if isinstance(key, int) else Points)(
            {
                dimension: self._get_broadcasted(coordinates)[key]
                for dimension, coordinates in self.coordinates.items()
            }
        )
        return result

    def __neg__(self):  # ()~Points
        result = Points(
            {
                dimension: -coordinates
                for dimension, coordinates in self.coordinates.items()
            },
            length=self._length,
        )
        return result

    def __add__(self, object):  # (object~object)~Points
        result = self._apply(operator.add, object)
        return result

    def __radd__(self, object):  # (object~object)~Points
        result = self + object
        return result

    def __iadd__(self, object):  # (object~object)~Points
        self._apply_inplace(operator.iadd, object)
        return self

    def __sub__(self, object):  # (object~object)~Points
        result = self._apply(operator.sub, object)
        return result

    def __isub__(self, object):  # (object~object)~Points
        self._apply_inplace(operator.isub, object)
        return self

    def __mul__(self, object):  # (object~object)~Points
        result = self._apply(operator.mul, object)
        return result

    def __rmul__(self, object):  # (object~object)~Points
        result = self * object
        return result

    def __imul__(self, object):  # (object~object)~Points
        self._apply_inplace(operator.imul, object)
        return self

    def __truediv__(self, object):  # (object~object)~Points
        result = self._apply(operator.truediv, object)
        return result

    def __itruediv__(self, object):  # (object~object)~Points
        self._apply_inplace(operator.itruediv, object)
        return self

    def _apply(self, function, object):
        if isinstance(object, (Point, Points)):
            if isinstance(object, Points):
                assert len(object) == len(self)  # TODO useful?

            result = Points(
                {
                    dimension: function(coordinates, object.coordinates[dimension])
                    for dimension, coordinates in self.coordinates.items()
                },
                length=self._length,
            )
            return result

        result = Points(
            {
                dimension: function(coordinates, object)
                for dimension, coordinates in self.coordinates.items()
            },
            length=self._length,
        )
        return result

    def _apply_inplace(self, function, object):
        if isinstance(object, (Point, Points)):
            self.coordinates.update(
                (dimension, function(coordinates, object.coordinates[dimension]))
                for dimension, coordinates in self.coordinates.items()
            )
            return

        self.coordinates.update(
            (dimension, function(coordinates, object))
            for dimension, coordinates in self.coordinates.items()
        )

    def __and__(self, object):  # (object~Points)~Points
        if isinstance(object, Points):
            assert len(object) == len(self)
            coordinates = self.coordinates.copy()
            coordinates.update(object.coordinates)
            result = Points(coordinates, length=self._length)
            return result

        raise TypeError(type(object))

    def get_sum(self):  # ()~Point
        result = Point(
            {
                dimension: self._get_broadcasted(coordinates).sum(axis=0)
                for dimension, coordinates in self.coordinates.items()
            }
        )
        return result

    def get_product(self):  # ()~Point
        result = Point(
            {
                dimension: self._get_broadcasted(coordinates).prod(axis=0)
                for dimension, coordinates in self.coordinates.items()
            }
        )
        return result

    def get_cartesian_product(self, points):  # (points~Points)~Points
        assert points.coordinates.keys().isdisjoint(self.coordinates.keys())
        coordinates = {
            dimension: numpy.repeat(self._get_broadcasted(coordinates), len(points))
            for dimension, coordinates in self.coordinates.items()
        }
        coordinates.update(
            (dimension, numpy.tile(points._get_broadcasted(coordinates), len(self)))
            for dimension, coordinates in points.coordinates.items()
        )
        result = Points(coordinates)
        return result

    def get_indices(self, point):  # (point~Point)~numpy.ndarray([index~int])
        result = None

        for dimension, coordinate in point.coordinates.items():
            if result is None:
                (result,) = numpy.nonzero(
                    self._get_broadcasted(self.coordinates[dimension]) == coordinate
                )

            else:
                result = result[
                    self._get_broadcasted(self.coordinates[dimension])[result]
                    == coordinate
                ]

        return result

    def get_dictionary(
        self,
    ):  # ()~{dimension~Dimension: coordinates~numpy.ndarray([coordinate~object])}
        result = {
            dimension: self._get_broadcasted(coordinates)
            for dimension, coordinates in self.coordinates.items()
        }
        return result

    def get_coordinates(
        self,
    ):  # ()~iter(coordinates~numpy.ndarray([coordinate~object]))
        result = (
            self._get_broadcasted(coordinates)
            for coordinates in self.coordinates.values()
        )
        return result

    def _get_broadcasted(self, array):
        result = (
            array
            if len(array) == self._length
            else numpy.broadcast_to(array, (self._length,))
        )
        return result

    def copy(self):  # ()~Points
        result = Points(self.coordinates.copy(), length=self.length)
        return result

    def __repr__(self):
        result = "Points({}, length={})".format(
            repr(self.coordinates), repr(self.length)
        )
        return result

    @staticmethod
    def stack(points):  # (points~iter(Points))~Points
        result = Points(
            d_batch.ArrayDictBatchInterface.concatenate_batches(
                {
                    dimension: points._get_broadcasted(coordinates)
                    for dimension, coordinates in points.coordinates.items()
                }
                for points in points
            )
        )
        return result


def pack_points(points):  # (points~iter(Point))~Points
    points = iter(points)

    first_point = next(points)

    coordinates = {
        dimension: [coordinate]
        for dimension, coordinate in first_point.coordinates.items()
    }

    for point in points:
        for dimension, coordinate in point.coordinates.items():
            coordinates[dimension].append(coordinate)

    result = Points(coordinates)
    return result


class Matrix:
    """
    - like (coordinates~numpy.ndarray([row~[coordinate~object]]), row dimensions~[Dimension], column dimensions~[Dimension])
    - [Dimension, Dimension]
      - returns object
    - [{Dimension}, Dimension], [Dimension, {Dimension}]
      - returns Point
    - [{Dimension}, {Dimension}]
      - returns Matrix
    - -
    - +-*/
    """

    def __init__(
        self, coordinates, row_dimensions, column_dimensions
    ):  # (coordinates~numpy.ndarray([row~[coordinate~object]]), row_dimensions~[Dimension], column_dimensions~[Dimension])~Matrix
        self.coordinates = coordinates
        self.row_dimensions = row_dimensions
        self.column_dimensions = column_dimensions

        self._row_dimensions = (
            row_dimensions
            if isinstance(row_dimensions, _Dimensions)
            else _Dimensions(row_dimensions)
        )
        self._column_dimensions = (
            column_dimensions
            if isinstance(column_dimensions, _Dimensions)
            else _Dimensions(column_dimensions)
        )

    def get_row_dimensions(self):  # ()~[Dimension]
        return self._row_dimensions

    def get_column_dimensions(self):  # ()~[Dimension]
        return self._column_dimensions

    def __getitem__(
        self, key
    ):  # (key~(row dimension~Dimension, column dimension~Dimension))~object
        # or (key~(row dimensions~{Dimension}, column dimension~Dimension) or (row dimension~Dimension, column dimensions~{Dimension}))~Point
        # or (key~(row dimensions~{Dimension}, column dimensions~{Dimension}))~Matrix

        if isinstance(key, tuple):
            row_key, column_key = key

            if isinstance(row_key, Dimension):
                row_index = self._row_dimensions.get_index(row_key)

                if isinstance(column_key, Dimension):
                    column_index = self._column_dimensions.get_index(column_key)
                    result = self.coordinates[row_index, column_index]
                    return result

                column_key = d_python_tools.get_sequence(column_key)
                column_indices = list(
                    map(self._column_dimensions.get_index, column_key)
                )

                result = Point(
                    dict(
                        zip(
                            column_key,
                            self.coordinates[row_index, column_indices],
                        )
                    )
                )
                return result

            else:
                if isinstance(column_key, Dimension):
                    row_key = d_python_tools.get_sequence(row_key)
                    row_indices = list(map(self._row_dimensions.get_index, row_key))
                    column_index = self._column_dimensions.get_index(column_key)

                    result = Point(
                        dict(
                            zip(
                                row_key,
                                self.coordinates[row_indices, column_index],
                            )
                        )
                    )
                    return result

                if self._row_dimensions.is_equivalent_to(
                    row_key
                ) and self._column_dimensions.is_equivalent_to(column_key):
                    return self

                row_key = d_python_tools.get_sequence(row_key)
                column_key = d_python_tools.get_sequence(column_key)

                row_indices = list(map(self._row_dimensions.get_index, row_key))
                column_indices = list(
                    map(self._column_dimensions.get_index, column_key)
                )
                result = Matrix(
                    self.coordinates[numpy.ix_(row_indices, column_indices)],
                    row_key,
                    column_key,
                )
                return result

        raise TypeError(type(key))

    def __neg__(self):  # ()~Matrix
        result = Matrix(
            -self.coordinates, self._row_dimensions, self._column_dimensions
        )
        return result

    def __add__(self, object):  # (object~object)~Matrix
        result = self._apply(operator.add, object)
        return result

    def __radd__(self, object):  # (object~object)~Matrix
        result = self + object
        return result

    def __iadd__(self, object):  # (object~object)~Matrix
        self._apply_inplace(operator.iadd, object)
        return self

    def __sub__(self, object):  # (object~object)~Matrix
        result = self._apply(operator.sub, object)
        return result
        
    def __rsub__(self, object):  # (object~object)~Matrix
      result = -self + object
      return result

    def __isub__(self, object):  # (object~object)~Matrix
        self._apply_inplace(operator.isub, object)
        return self

    def __mul__(self, object):  # (object~object)~Matrix
        result = self._apply(operator.mul, object)
        return result

    def __rmul__(self, object):  # (object~object)~Matrix
        result = self * object
        return result

    def __imul__(self, object):  # (object~object)~Matrix
        self._apply_inplace(operator.imul, object)
        return self

    def __truediv__(self, object):  # (object~object)~Matrix
        result = self._apply(operator.truediv, object)
        return result

    def __itruediv__(self, object):  # (object~object)~Matrix
        self._apply_inplace(operator.itruediv, object)
        return self

    def _apply(self, function, object):
        if isinstance(object, Matrix):
            result = Matrix(
                function(
                    self.coordinates,
                    object[self._row_dimensions, self._column_dimensions].coordinates,
                ),
                self._row_dimensions,
                self._column_dimensions,
            )
            return result

        result = Matrix(
            function(self.coordinates, object),
            self._row_dimensions,
            self._column_dimensions,
        )
        return result

    def _apply_inplace(self, function, object):
        if isinstance(object, Matrix):
            self.coordinates = function(
                self.coordinates,
                object[self._row_dimensions, self._column_dimensions].coordinates,
            )
            return

        self.coordinates = function(self.coordinates, object)
        
    if False:
      def __matmul__(self, object):  # (object~Point or Points or Matrix or Matrices)
        if isinstance(object, (Point, Points, Matrix)):
          pass
          
        result = object @ self
        return result

    def __repr__(self):
        result = "Matrix({}, {}, {})".format(
            repr(self.coordinates),
            repr(self.row_dimensions),
            repr(self.column_dimensions),
        )
        return result


if False:
  def _get_matrix_multiplication(left, right):
    #if isinstance(left, Point) and isinstance(right, Points):
    #  left = left.get_points(len(right))
    
    # TODO check compatibility
      
    dimensions = list(left.get_dimensions())
    left_array = left.get_array(dimensions)
    right_array = right.get_array(dimensions)
    
    result = left_array @ right_array
    
    if len(result.shape) == 0:
      return result
      
    if len(result.shape) == 1:
      if isinstance(left, Matrix) or isinstance(right, Matrix):
        pass
  


def get_matrix(dictionary):
    """
    - transforms {row dimension~Dimension: {column dimensions~Dimension: coordinate~object}} into Matrix
    """

    row_dimensions = list(dictionary.keys())
    column_dimensions = list(next(iter(dictionary.values())).keys())
    coordinates = numpy.array(
        [
            [row_dictionary[column_dimension] for column_dimension in column_dimensions]
            for row_dictionary in (
                dictionary[row_dimension] for row_dimension in row_dimensions
            )
        ]
    )
    result = Matrix(coordinates, row_dimensions, column_dimensions)
    return result


class Matrices:
    """
    - like (coordinates~numpy.ndarray([matrix~[row~[coordinate~object]]]), row dimensions~[Dimension], column dimensions~[Dimension])
    - iterable
      - yields Matrix
    - [Dimension, Dimension]
      - returns numpy.ndarray([coordinate~object])
    - [{Dimension}, Dimension], [Dimension, {Dimension}]
      - returns Points
    - [{Dimension}, {Dimension}]
      - returns Matrices
    - [int]
      - returns Matrix
    - [slice]
      - returns Matrices
    - +-*/
    """

    def __init__(
        self, coordinates, row_dimensions, column_dimensions
    ):  # (coordinates~numpy.ndarray([matrix~[row~[coordinate~object]]]), row_dimensions~[Dimension], column_dimensions~[Dimension])
        self.coordinates = coordinates
        self.row_dimensions = row_dimensions
        self.column_dimensions = column_dimensions

        self._row_dimensions = (
            row_dimensions
            if isinstance(row_dimensions, _Dimensions)
            else _Dimensions(row_dimensions)
        )
        self._column_dimensions = (
            column_dimensions
            if isinstance(column_dimensions, _Dimensions)
            else _Dimensions(column_dimensions)
        )

    def get_row_dimensions(self):  # ()~[Dimension]
        return self._row_dimensions

    def get_column_dimensions(self):  # ()~[Dimension]
        return self._column_dimensions

    def __len__(self):
        result = self.coordinates.shape[0]
        return result

    def __iter__(self):  # ()~iter(Matrix)
        result = (
            Matrix(coordinates, self._row_dimensions, self._column_dimensions)
            for coordinates in self.coordinates
        )
        return result

    def __getitem__(
        self, key
    ):  # (key~(row dimension~Dimension, column dimension~Dimension))~numpy.ndarray([coordinate~object])
        # or (key~(row dimensions~{Dimension}, column dimension~Dimension) or (row dimension~Dimension, column dimensions~{Dimension}))~Points
        # or (key~(row dimensions~{Dimension}, column dimensions~{Dimension}))~Matrices
        # or (key~int)~Matrix
        # or (key~object)~Matrices

        if isinstance(key, tuple):
            row_key, column_key = key

            if isinstance(row_key, d_base.Dimension):
                row_index = self._row_dimensions.get_index(row_key)

                if isinstance(column_key, d_base.Dimension):
                    column_index = self._column_dimensions.get_index(column_key)
                    result = self.coordinates[:, row_index, column_index]
                    return result

                column_key = d_python_tools.get_sequence(column_key)
                column_indices = list(
                    map(self._column_dimensions.get_index, column_key)
                )

                result = Points(
                    dict(
                        zip(
                            column_key,
                            numpy.transpose(
                                self.coordinates[:, row_index, column_indices]
                            ),
                        )
                    ),
                    length=len(self),
                )
                return result

            else:
                if isinstance(column_key, d_base.Dimension):
                    row_key = d_python_tools.get_sequence(row_key)
                    row_indices = list(map(self._row_dimensions.get_index, row_key))
                    column_index = self._column_dimensions.get_index(column_key)

                    result = Points(
                        dict(
                            zip(
                                row_key,
                                numpy.transpose(
                                    self.coordinates[:, row_indices, column_index]
                                ),
                            )
                        ),
                        length=len(self),
                    )
                    return result

                if self._row_dimensions.is_equivalent_to(
                    row_key
                ) and self._column_dimensions.is_equivalent_to(column_key):
                    return self

                row_key = d_python_tools.get_sequence(row_key)
                column_key = d_python_tools.get_sequence(column_key)

                row_indices = list(map(self._row_dimensions.get_index, row_key))
                column_indices = list(
                    map(self._column_dimensions.get_index, column_key)
                )
                result = Matrix(
                    self.coordinates[
                        numpy.ix_(
                            range(self.coordinates.shape[0]),
                            row_indices,
                            column_indices,
                        )
                    ],
                    row_key,
                    column_key,
                )
                return result

        result = (Matrix if isinstance(key, int) else Matrices)(
            self.coordinates[key, ...], self._row_dimensions, self._column_dimensions
        )
        return result

    def __add__(self, object):  # (object~object)~Matrices
        result = self._apply(operator.add, object)
        return result

    def __radd__(self, object):  # (object~object)~Matrices
        result = self + object
        return result

    def __iadd__(self, object):  # (object~object)~Matrices
        self._apply_inplace(operator.iadd, object)
        return self

    def __sub__(self, object):  # (object~object)~Matrices
        result = self._apply(operator.sub, object)
        return result

    def __isub__(self, object):  # (object~object)~Matrices
        self._apply_inplace(operator.isub, object)
        return self

    def __mul__(self, object):  # (object~object)~Matrices
        result = self._apply(operator.mul, object)
        return result

    def __rmul__(self, object):  # (object~object)~Matrices
        result = self * object
        return result

    def __imul__(self, object):  # (object~object)~Matrices
        self._apply_inplace(operator.imul, object)
        return self

    def __truediv__(self, object):  # (object~object)~Matrices
        result = self._apply(operator.truediv, object)
        return result

    def __itruediv__(self, object):  # (object~object)~Matrices
        self._apply_inplace(operator.itruediv, object)
        return self

    def _apply(self, function, object):
        if isinstance(object, numpy.ndarray):
            coordinates = function(self.coordinates, numpy.reshape(object, (-1, 1, 1)))
            result = Matrices(
                coordinates, self._row_dimensions, self._column_dimensions
            )
            return result

        result = Matrices(
            function(self.coordinates, object),
            self._row_dimensions,
            self._column_dimensions,
        )
        return result

    def _apply_inplace(self, function, object):
        if isinstance(object, numpy.ndarray):
            self.coordinates = function(
                self.coordinates, numpy.reshape(object, (-1, 1, 1))
            )
            return

        self.coordinates = function(self.coordinates, object)

    def get_sum(self):  # ()~Matrix
        result = Matrix(
            numpy.sum(self.coordinates, axis=0),
            self._row_dimensions,
            self._column_dimensions,
        )
        return result

    def get_product(self):  # ()~Matrix
        result = Matrix(
            numpy.prod(self.coordinates, axis=0),
            self._row_dimensions,
            self._column_dimensions,
        )
        return result

    def __repr__(self):
        result = "Matrices({}, {}, {})".format(
            repr(self.coordinates),
            repr(self.row_dimensions),
            repr(self.column_dimensions),
        )
        return result


def get_matrices(dictionary):
    """
    - transforms {row dimension~Dimension: {column dimension~Dimension: coordinates~numpy.ndarray([coordinate~object])}} into Matrices
    """

    row_dimensions = list(dictionary.keys())
    column_dimensions = list(next(iter(dictionary.values())).keys())
    coordinates = numpy.transpose(
        numpy.array(
            [
                [
                    row_dictionary[column_dimension]
                    for column_dimension in column_dimensions
                ]
                for row_dictionary in (
                    dictionary[row_dimension] for row_dimension in row_dimensions
                )
            ]
        ),
        axes=[2, 0, 1],
    )
    result = Matrices(coordinates, row_dimensions, column_dimensions)
    return result


def pack_matrices(matrices):  # (matrices~iter(Matrix))~Matrices
    matrices = iter(matrices)

    first_matrix = next(matrices)
    row_dimensions = first_matrix.get_row_dimensions()
    column_dimensions = first_matrix.get_column_dimensions()

    coordinates = [first_matrix.coordinates]
    coordinates.extend(
        matrix[row_dimensions, column_dimensions].coordinates for matrix in matrices
    )
    coordinates = numpy.stack(coordinates)

    result = Matrices(coordinates, row_dimensions, column_dimensions)
    return result
