import docopylae.numeric as d_numeric
import bokeh.models as b_models
import bokeh.plotting as b_plotting
import numpy


# import docopylae.distribution.base as dd_base

# POINT = dd_base.POINT
# POINTS = dd_base.POINTS


def get_design_plot(
    approximate_design, sensitivity=None, sensitivity_points=None
):  # (approximate_design~d_base.ApproximateDesign, sensitivity~d_base.Sensitivity or None, sensitivity_points~POINTS or None)~b_plotting.Figure
    point_weights = iter(approximate_design.get_point_weights())

    control_point, weight = next(point_weights)

    if len(control_point.coordinates) == 1:
        (dimension,) = control_point.coordinates.keys()
        figure = b_plotting.Figure(
            x_axis_label=dimension.id, y_axis_label="weight", y_range=(-0.05, 1.05)
        )

        (coordinate,) = control_point.coordinates.values()
        design_coordinates = [coordinate]
        weights = [weight]

        for control_point, weight in point_weights:
            (coordinate,) = control_point.coordinates.values()
            design_coordinates.append(coordinate)
            weights.append(weight)

        design_coordinates = numpy.array(design_coordinates)
        weights = numpy.array(weights)

        figure.segment(
            x0=design_coordinates,
            y0=[0] * len(design_coordinates),
            x1=design_coordinates,
            y1=weights,
            line_width=2,
        )

        if sensitivity is not None:
            sensitivity = sensitivity.get_specialization(d_numeric)

            (coordinates,) = sensitivity_points.get_coordinates()
            sensitivities = sensitivity.evaluate_many(
                sensitivity_points, approximate_design
            )

            order = numpy.argsort(coordinates)
            coordinates = coordinates[order]
            sensitivities = sensitivities[order]

            figure.extra_y_ranges["right"] = b_models.DataRange1d(0, None)
            figure.add_layout(
                b_models.LinearAxis(y_range_name="right", axis_label="sensitivity"),
                "right",
            )
            figure.line(coordinates, sensitivities, y_range_name="right")

            figure.cross(
                design_coordinates,
                numpy.interp(
                    design_coordinates,
                    coordinates,
                    sensitivities,
                    left=float("nan"),
                    right=float("nan"),
                ),
                line_alpha=weights / weights.max(),
                size=8,
                y_range_name="right",
            )

        return figure

    raise NotImplementedError(type(self))
