import docopylae.base as d_base
import docopylae.python_tools as d_python_tools
import docopylae.sum as d_sum
import docopylae.sum.base as ds_base
import docopylae.sum.transform as ds_transform
import numpy
import scipy.optimize as s_optimize
import scipy.spatial as s_spatial
import random


del sum  # workaround import behaviour


# import docopylae.distribution.base as dd_base
# import docopylae.space as d_space
# import docopylae.sum.transform.base as dst_base

# POINT = dd_base.POINT
# POINTS = dd_base.POINTS


class DirectFisherInformation(d_base.FisherInformation):
    def __init__(
        self, second_derivative, distribution, sum_transformers=None, cache=None
    ):  # (second_derivative~d_base.SecondDerivative, distribution~dd_base.ProbabilityDistribution, sum_transformers~[dst_base.SumTransformer], cache~{} or None)~DirectFisherInformation
        super().__init__()

        import docopylae.numeric

        self.second_derivative = second_derivative
        self.distribution = distribution
        self.sum_transformers = sum_transformers
        self.cache = cache

    def evaluate(self, point):  # (point~POINT)~d_space.Matrix
        import docopylae.numeric as d_numeric

        if self.cache is not None:
            result = self.cache.get(point, None)
            if result is not None:
                return result

        inner_function = DirectFisherInformation._InnerFunction(point, self)

        sum = d_sum.Sum(
            [d_sum.SubSum(inner_function, self.distribution.get_sample_space())]
        )

        conditional_random_variables = (
            self.distribution.get_conditional_random_variables()
        )
        probability_integral_transformation = (
            ds_transform.ProbabilityIntegralTransformation(
                {
                    random_variable: (
                        self.distribution.get_marginal_distribution(
                            {random_variable}, conditional_random_variables
                        ).get_specialization(d_numeric),
                        point,
                    )
                    for random_variable in self.distribution.get_random_variables()
                }
            )
        )
        sum_transformers = [
            ds_transform.ContinuousTransformer([probability_integral_transformation])
        ]
        if self.sum_transformers is not None:
            sum_transformers.extend(self.sum_transformers)

        sum = ds_transform.TransformedSum(sum, sum_transformers)

        result = -sum.evaluate()

        if self.cache is not None:
            self.cache[point] = result

        return result

    class _InnerFunction(ds_base.Function):
        def __init__(self, point, direct_fi):
            super().__init__()

            self.point = point
            self.direct_fi = direct_fi

        def evaluate_many(self, points):  # (points~POINTS)~d_space.Matrices
            import docopylae.numeric as d_numeric

            if False:
                print()
                print(self.point)
                print(self.point.get_points(len(points)))
            points = points & self.point.get_points(len(points))
            if False:
                print(points)

            if False:  # TODO remove
                print()
                print(points)
                a = self.direct_fi.second_derivative.evaluate_many(points)
                print(a)
                b = self.direct_fi.distribution.get_specialization(
                    d_numeric
                ).get_probabilities(points)
                print(b)
            result = self.direct_fi.second_derivative.evaluate_many(
                points
            ) * self.direct_fi.distribution.get_specialization(
                d_numeric
            ).get_probabilities(
                points
            )
            return result


class ApproximateDesign(d_base.ApproximateDesign):
    def __init__(
        self, control_points, weights
    ):  # (control_points~tuple(control point~POINT), weights~[weight~object])
        super().__init__()

        self.control_points = control_points
        self.weights = weights

    def get_point_weights(
        self,
    ):  # iter(point weight~(control point~POINT, weight~object))
        result = zip(self.control_points, self.weights)
        return result

    def __repr__(self):
        point_strings = [repr(control_point) for control_point in self.control_points]
        maximum_length = max(len(string) for string in point_strings)
        format_string = "{{:{}}}: {{}},".format(maximum_length)
        result = "ApproximateDesign({{\n  {}\n}})".format(
            "\n  ".join(
                format_string.format(
                    point_string,
                    repr(weight),
                )
                for point_string, weight in zip(point_strings, self.weights)
            )
        )
        return result


class DCriterion(d_base.Criterion):
    def __init__(
        self, point, fisher_information
    ):  # (point~POINT, fisher_information~d_base.FisherInformation)~DCriterion
        super().__init__()

        self.point = point
        self.fisher_information = fisher_information

    def get_sensitivity(self):  # ()~DSensitivity
        result = DSensitivity(self)
        return result


def _evaluate_fisher_information(control_point, point, fisher_information):
    result = fisher_information.evaluate(point & control_point)
    return result


class DSensitivity(d_base.Sensitivity):
    def __init__(self, d_criterion):  # (d_criterion~DCriterion)~DSensitivity
        super().__init__()

        self.d_criterion = d_criterion

    # evaluate(control_point~POINT, design~d_base.ApproximateDesign)~object

    # evaluate_many(control_points~POINTS, design~d_base.ApproximateDesign)~[sensitivity~object]


class WynnDesignOptimizer(d_base.DesignOptimizer):
    def __init__(
        self, criterion, design_space, n_iterations=int(1e4)
    ):  # (criterion~d_base.Criterion, design_space~ds_base.Space, n_iterations~int)~WynnDesignOptimizer
        super().__init__()

        import docopylae.numeric

        self.criterion = criterion
        self.design_space = design_space
        self.n_iterations = n_iterations

    def optimize(
        self, design=None
    ):  # (design~d_base.ApproximateDesign or None)~ApproximateDesign
        import docopylae.numeric as d_numeric

        sensitivity = self.criterion.get_sensitivity().get_specialization(d_numeric)

        control_points = self.design_space.get_points()

        if design is None:
            weights = numpy.full(
                (len(control_points),), 1 / len(control_points), dtype=float
            )

        else:
            weights = numpy.zeros((len(control_points),), dtype=float)

            for control_point, weight in design.get_point_weights():
                (index,) = control_points.get_indices(control_point)
                weights[index] = weight

        result = ApproximateDesign(control_points, weights)

        with sensitivity.cache(2):
            for iteration in range(self.n_iterations):
                sensitivities = sensitivity.evaluate_many(control_points, result)
                max_sensitivity = numpy.max(sensitivities)
                (max_indices,) = numpy.nonzero(sensitivities == max_sensitivity)

                max_index = random.choice(max_indices)

                new_weight = weights[max_index] + 1 / (iteration + 2)
                if 1 < new_weight:
                    new_weight = 1
                scaling_factor = (1 - new_weight) / (weights.sum() - weights[max_index])
                weights *= scaling_factor
                weights[max_index] = new_weight

        return result


def reduce_design(
    approximate_design, criterion, max_distance
):  # (approximate_design~docopylae.ApproximateDesign, criterion~d_base.Criterion, max_distance~float or None)~d_base.ApproximateDesign
    import docopylae.numeric as d_numeric

    criterion = criterion.get_specialization(d_numeric)

    result = approximate_design
    result_criterion_value = criterion.evaluate(result)

    # remove low weights

    base_design = result
    indices = numpy.argsort(result.weights)
    start_index = 0
    stop_index = len(indices)

    while 2 <= (stop_index - start_index):
        index = (start_index + stop_index) // 2

        mask = indices[index:]
        weights = base_design.weights[mask]
        weights *= 1 / weights.sum()
        approximate_design = ApproximateDesign(
            base_design.control_points[mask], weights
        )

        criterion_value = criterion.evaluate(approximate_design)

        if result_criterion_value <= criterion_value:
            result = approximate_design
            result_criterion_value = criterion_value

            start_index = index

        else:
            stop_index = index

    # merge points

    if max_distance is not None:
        while 2 <= len(result.control_points):
            coordinates = numpy.transpose(
                numpy.stack(list(result.control_points.get_coordinates()))
            )
            distances = s_spatial.distance_matrix(coordinates, coordinates)
            numpy.fill_diagonal(distances, float("inf"))

            for keep_index, remove_index in numpy.argwhere(distances <= max_distance):
                keep_index = int(keep_index)
                remove_index = int(remove_index)

                if remove_index < keep_index:
                    keep_index, remove_index = remove_index, keep_index

                point_1 = result.control_points[keep_index]
                point_2 = result.control_points[remove_index]
                weight = result.weights[keep_index] + result.weights[remove_index]

                mask = numpy.ones((len(result.control_points),), dtype=bool)
                mask[remove_index] = False

                approximate_design = ApproximateDesign(
                    result.control_points[mask], result.weights[mask]
                )
                approximate_design.weights[keep_index] = weight

                def _set_point(a):
                    point = point_1 * a + point_2 * (1 - a)
                    for dimension, coordinate in point.coordinates.items():
                        approximate_design.control_points.coordinates[dimension][
                            keep_index
                        ] = coordinate

                def _evaluate_criterion(a):
                    _set_point(a)
                    result = -criterion.evaluate(approximate_design)
                    return result

                _ = s_optimize.minimize_scalar(
                    _evaluate_criterion, bounds=(0, 1), method="Bounded"
                )
                criterion_value = -_.fun

                if criterion_value < result_criterion_value:
                    continue

                _set_point(_.x)

                result = approximate_design
                result_criterion_value = criterion_value
                break

            else:
                break

    return result
