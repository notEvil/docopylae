import docopylae
import docopylae.base as d_base
import docopylae.numeric as d_numeric
import docopylae.numeric.base as dn_base
import docopylae.python_tools as d_python_tools
import docopylae.space as d_space
import numpy
import numpy.linalg as n_linalg
import collections
import contextlib
import threading
import weakref


# import docopylae.numeric.distribution.base as dnd_base

# POINT = dnd_base.POINT
# POINTS = dnd_base.POINTS


def initialize():  # ()~None
    import docopylae.numeric.distribution as dn_distribution

    docopylae.DCriterion.set_specialization_type(d_numeric, DCriterion)
    docopylae.DSensitivity.set_specialization_type(d_numeric, DSensitivity)

    dn_distribution.initialize()


class DCriterion(dn_base.Criterion):
    def evaluate(self, design):  # (design~d_base.ApproximateDesign)~number
        point = self.specializable.point
        fisher_information = self.specializable.fisher_information

        result = n_linalg.det(
            d_python_tools.get_sum(
                docopylae._evaluate_fisher_information(
                    control_point, point, fisher_information
                )
                * weight
                for control_point, weight in design.get_point_weights()
            ).coordinates
        )
        return result


class DSensitivity(dn_base.Sensitivity):
    def __init__(
        self, specializable
    ):  # (specializable~docopylae.DSensitivity)~DSensitivity
        super().__init__(specializable)

        self._fisher_information_cache = weakref.WeakValueDictionary()

        self._cache = None

    def evaluate(
        self, control_point, design
    ):  # (control_point~POINT, design~d_base.ApproximateDesign)~number
        design_fi = self._get_design_fi(design)
        fisher_information = self._get_fisher_information(control_point)

        design_fi = design_fi[
            fisher_information.get_row_dimensions(),
            fisher_information.get_column_dimensions(),
        ]

        (result,) = numpy.matmul(
            numpy.reshape(n_linalg.inv(design_fi.coordinates), (1, -1)),
            numpy.reshape(fisher_information.coordinates, (-1, 1)),
        )
        return result

    def evaluate_many(
        self, control_points, design
    ):  # (control_points~POINTS, design~d_base.ApproximateDesign)~numpy.ndarray([sensitivity~number])
        design_fi = self._get_design_fi(design)
        fisher_informations = self._get_fisher_informations(control_points)

        design_fi = design_fi[
            fisher_informations.get_row_dimensions(),
            fisher_informations.get_column_dimensions(),
        ]

        (result,) = numpy.matmul(
            numpy.reshape(
                n_linalg.inv(design_fi.coordinates), (1, -1)
            ),  # flatten design fisher information
            numpy.transpose(
                numpy.reshape(
                    fisher_informations.coordinates,
                    (-1, design_fi.coordinates.size),
                )  # flatten fisher informations
            ),
        )

        return result

    def _get_design_fi(self, approximate_design):
        if isinstance(approximate_design, docopylae.ApproximateDesign):
            fisher_informations = self._get_fisher_informations(
                approximate_design.control_points
            )
            result = (fisher_informations * approximate_design.weights).get_sum()
            return result

        result = d_python_tools.get_sum(
            self._get_fisher_information(control_point) * weight
            for control_point, weight in approximate_design.get_point_weights()
        )
        return result

    def _get_fisher_informations(self, control_points):
        if self._cache is not None:
            result = self._cache.get(control_points)
            if result is not None:
                return result

        result = d_space.pack_matrices(
            self._get_fisher_information(control_point)
            for control_point in control_points
        )
        if self._cache is not None:
            self._cache.set(control_points, result)
        return result

    def _get_fisher_information(self, control_point):
        result = self._fisher_information_cache.get(control_point, None)
        if result is not None:
            return result

        result = docopylae._evaluate_fisher_information(
            control_point,
            self.specializable.d_criterion.point,
            self.specializable.d_criterion.fisher_information,
        )
        self._fisher_information_cache[control_point] = result
        return result

    @contextlib.contextmanager
    def cache(self, size):
        self._cache = _Cache(size)

        try:
            yield

        finally:
            self._cache = None


class _Cache:
    def __init__(self, size):
        super().__init__()

        self.size = size

        self._lock = threading.Lock()
        self._keys = collections.deque([], size)
        self._objects = collections.deque([], size)

    def get(self, key):
        with self._lock:
            try:
                index = self._keys.index(key)

            except ValueError:
                return None

            result = self._objects[index]
            return result

    def set(self, key, object):
        with self._lock:
            try:
                index = self._keys.index(key)

            except ValueError:
                self._keys.appendleft(key)
                self._objects.appendleft(object)
                return

            self._objects[index] = object
