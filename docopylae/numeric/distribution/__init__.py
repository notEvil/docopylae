import docopylae.distribution as d_distribution
import docopylae.numeric as d_numeric
import docopylae.numeric.distribution.base as dnd_base
import docopylae.python_tools as d_python_tools
import docopylae.simpson as d_simpson
import docopylae.space as d_space
import numpy
import numpy.linalg as n_linalg
import scipy.special as s_special
import scipy.stats as s_stats


# POINT = dnd_base.POINT
# POINTS = dnd_base.POINTS


def initialize():  # ()~None
    import docopylae.numeric.distribution.copula as dnd_copula

    dnd_base.initialize()

    d_distribution.UnivariateUniformDistribution.set_specialization_type(
        d_numeric, UnivariateUniformDistribution
    )
    d_distribution.UnivariateNormalDistribution.set_specialization_type(
        d_numeric, UnivariateNormalDistribution
    )
    if False:
        d_distribution.GeometricHyperbolicSkewTDistribution.set_specialization_type(
            d_numeric, GeometricHyperbolicSkewTDistribution
        )
    d_distribution._MarginalGeometricHyperbolicSkewTDistribution.set_specialization_type(
        d_numeric, _MarginalGeometricHyperbolicSkewTDistribution
    )

    dnd_copula.initialize()


class UnivariateUniformDistribution(dnd_base.ProbabilityDistribution):
    def get_begin_values(
        self, points
    ):  # (points~POINTS)~numpy.ndarray([begin value~number])
        result = numpy.zeros((len(points),))
        return result

    def get_end_values(
        self, points
    ):  # (points~POINTS)~numpy.ndarray([begin value~number])
        result = numpy.ones((len(points),))
        return result

    # probabilities

    def get_probabilities(
        self, points
    ):  # (points~POINTS)~numpy.ndarray([probability~number])
        result = numpy.ones((len(points),))
        return result

    def get_cumulative_probabilities(
        self, points
    ):  # (points~POINTS)~[cumulative probability~object]
        (random_variable,) = self.get_random_variables()
        begin_values = self.get_begin_values(points)
        end_values = self.get_end_values(points)
        result = (points[random_variable] - begin_values) / (end_values - begin_values)
        return result

    def get_quantiles(
        self, cumulative_probabilities, points
    ):  # (cumulative_probabilities~[cumulative probability~object], points~POINTS)~POINTS
        (random_variable,) = self.get_random_variables()
        begin_values = self.get_begin_values(points)
        end_values = self.get_end_values(points)
        result = d_space.Points(
            {
                random_variable: cumulative_probabilities * (end_values - begin_values)
                + begin_values
            },
            length=len(points),
        )
        return result


class UnivariateNormalDistribution(dnd_base.ProbabilityDistribution):

    # probabilities

    def get_probabilities(
        self, points
    ):  # (points~POINTS)~numpy.ndarray([probability~number])
        (random_variable,) = self.get_random_variables()
        outcomes = points[random_variable]
        (means,) = self.get_means(points).get_coordinates()
        (standard_deviations,) = self.get_standard_deviations(points).get_coordinates()
        result = (
            s_stats.norm._pdf((outcomes - means) / standard_deviations)
            / standard_deviations
        )
        if False:  # TODO remove
            print()
            print(points)
            print(means)
            print(standard_deviations)
            print(result)
        return result

    def get_cumulative_probability(self, point):  # (point~POINT)~number
        (random_variable,) = self.get_random_variables()
        outcome = point[random_variable]
        (mean,) = self.get_mean(point).coordinates.values()
        (standard_deviation,) = self.get_standard_deviation(point).coordinates.values()
        result = s_stats.norm._cdf((outcome - mean) / standard_deviation)
        return result

    def get_cumulative_probabilities(
        self, points
    ):  # (points~POINTS)~numpy.ndarray([cumulative probability~number])
        (random_variable,) = self.get_random_variables()
        outcomes = points[random_variable]
        (means,) = self.get_means(points).get_coordinates()
        (standard_deviations,) = self.get_standard_deviations(points).get_coordinates()
        result = s_stats.norm._cdf((outcomes - means) / standard_deviations)
        return result

    def get_quantiles(
        self, cumulative_probabilities, points
    ):  # (cumulative_probabilities~numpy.ndarray([cumulative probability~number]), points~POINTS)~POINTS
        (random_variable,) = self.get_random_variables()
        (means,) = self.get_means(points).get_coordinates()
        (standard_deviations,) = self.get_standard_deviations(points).get_coordinates()
        result = d_space.Points(
            {
                random_variable: s_stats.norm._ppf(cumulative_probabilities)
                * standard_deviations
                + means
            },
            length=len(points),
        )
        return result


if False:

    class GeometricHyperbolicSkewTDistribution(dnd_base.ProbabilityDistribution):
        pass


class _MarginalGeometricHyperbolicSkewTDistribution(dnd_base.ProbabilityDistribution):
    def __init__(
        self, specializable
    ):  # (specializable~d_python_tools.Specializable)~_MarginalGeometricHyperbolicSkewTDistribution
        super().__init__(specializable)

        self._conditional_random_variables = None

    def _get_conditional_random_variables(self):
        if self._conditional_random_variables is None:
            self._conditional_random_variables = d_python_tools.get_set(
                self.specializable.conditional_random_variables
            ) - d_python_tools.get_set(
                self.specializable.unconditional_distribution.get_conditional_random_variables()
            )

        return self._conditional_random_variables

    def _get_mu(self, point):
        unconditional_distribution = (
            self.specializable.unconditional_distribution.get_specialization(d_numeric)
        )
        random_variables = self.specializable.random_variables
        conditional_random_variables = self._get_conditional_random_variables()
        mu = unconditional_distribution.get_mu(point)

        mu_2 = mu[random_variables]

        if len(conditional_random_variables) == 0:
            return mu_2

        sigma = unconditional_distribution.get_sigma(point)

        result = mu[random_variables] + sigma[
            random_variables, conditional_random_variables
        ] @ sigma[
            conditional_random_variables, conditional_random_variables
        ].get_inverse() @ (
            point[conditional_random_variables] - mu[conditional_random_variables]
        )
        return result

    def _get_tau(self, point):
        unconditional_distribution = (
            self.specializable.unconditional_distribution.get_specialization(d_numeric)
        )
        random_variables = self.specializable.random_variables
        conditional_random_variables = self._get_conditional_random_variables()
        tau = unconditional_distribution.get_tau(point)

        tau_2 = tau[random_variables]

        if len(conditional_random_variables) == 0:
            return tau_2

        sigma = unconditional_distribution.get_sigma(point)

        result = (
            tau_2
            - sigma[random_variables, conditional_random_variables]
            @ sigma[
                conditional_random_variables, conditional_random_variables
            ].get_inverse()
            @ tau[conditional_random_variables]
        )
        return result
        
    def _get_sigma(self, point):
        unconditional_distribution = (
            self.specializable.unconditional_distribution.get_specialization(d_numeric)
        )
        random_variables = self.specializable.random_variables
        conditional_random_variables = self._get_conditional_random_variables()
        sigma = unconditional_distribution.get_sigma(point)
        
        sigma_22 = sigma[random_variables, random_variables]
        
        if len(conditional_random_variables) == 0:
          return sigma_22
          
        _ = sigma[conditional_random_variables, random_variables]
        result = sigma_22 - _.get_transposed() @ sigma[conditional_random_variables, conditional_random_variables].get_inverse() @ _
        return result
        
    def _get_lambda(self, point):
        unconditional_distribution = (
            self.specializable.unconditional_distribution.get_specialization(d_numeric)
        )
        conditional_random_variables = self._get_conditional_random_variables()
        lambda_ = unconditional_distribution.get_lambda(point)
        
        result = lambda_ - len(conditional_random_variables) / 2
        return result
    
    def _get_chi(self, point):
        unconditional_distribution = (
            self.specializable.unconditional_distribution.get_specialization(d_numeric)
        )
        conditional_random_variables = self._get_conditional_random_variables()
        chi = unconditional_distribution.get_chi(point)
        
        if len(conditional_random_variables) == 0:
          return chi
        
        mu = unconditional_distribution.get_mu(point)
        sigma = unconditional_distribution.get_sigma(point)

        _ = point[conditional_random_variables] - mu[conditional_random_variables]
        result = chi + _.get_transposed() @ sigma[conditional_random_variables, conditional_random_variables].get_inverse() @ _
        return result
        
    # statistics

    def get_mean(self, point):  # (point~POINT)~POINT
        # TODO fix for conditional
        mu = self._get_mu(point)
        tau = self._get_tau(point)
        lambda_ = self._get_lambda(point)
        chi = self._get_chi(point)

        if False:
            psi = self._get_psi(point)

            _ = numpy.sqrt(chi * psi)
            result = mu + tau * (chi / psi) * s_special.kv(
                lambda_ + 1, _
            ) / s_special.kv(lambda_, _)

        result = mu + tau * ((chi / 2) / (-lambda_ - 1))
        return result

    def get_standard_deviation(self, point):  # (point~POINT)~POINT
        # TODO fix for conditional
        mu = self._get_mu(point)
        tau = self._get_tau(point)
        sigma = self._get_sigma(point)
        lambda_ = self._get_lambda(point)
        chi = self._get_chi(point)

        if False:
            psi = self._get_psi(point)

            _a = numpy.sqrt(chi * psi)
            _b = s_special.kv(lambda_, _a)
            m1 = (chi / psi) * s_special.kv(lambda_ + 1, _a) / _b
            m2 = (chi / psi) ** 2 * s_special.kv(lambda_ + 2, _a) / _b
            result = numpy.diagonal(
                m1 * sigma + (m2 - m1 ** 2) * tau @ numpy.transpose(tau)
            )

        matrix = ((chi / 2) / (-lambda_ - 1)) * sigma + (
            (chi / 2) ** 2 / ((-lambda_ - 1) ** 2 * (-lambda_ - 2))
        ) * tau @ tau.get_transposed()
        
        random_variable, = matrix.get_row_dimensions()
        coordinate = numpy.reshape(matrix.coordinates, tuple())
        result = d_space.Point({random_variable: coordinate})
        return result

    # probabilities

    def get_quantile(
        self, cumulative_probability, point
    ):  # (cumulative_probability~object, point~POINT)~POINT
        (random_variable,) = self.specializable.random_variables

        mean, = self.get_mean(point).coordinates.values()
        standard_deviation, = self.get_standard_deviation(point).coordinates.values()
        get_probability = lambda object: self.get_probability(
            point & d_space.Point({random_variable: object})
        )
        approximate_integral = d_simpson.InfiniteApproximateIntegral(
            -float("inf"),
            mean - 3 * standard_deviation,
            mean + 3 * standard_deviation,
            float("inf"),
            get_probability,
        )

        result = d_space.Point(
            {
                random_variable: approximate_integral.evaluate_inverse(
                    cumulative_probability
                )
            }
        )
        return result

    def get_quantiles(
        self, cumulative_probabilities, points
    ):  # (cumulative_probabilities~numpy.ndarray([cumulative probability~object]), points~POINTS)~POINTS
        result = self._to_numpy(
            d_space.pack_points(
                self.get_quantile(cumulative_probability, point)
                for cumulative_probability, point in zip(
                    cumulative_probabilities, points
                )
            )
        )
        return result


if False:

    def _get_gh_p_probability(x, mu, tau, sigma, lambda_, chi, psi, p):
        _e1 = n_linalg.inv(sigma)
        _e2 = x - mu
        _e3 = p / 2
        _e4 = psi + numpy.transpose(tau) @ _e1 @ tau
        _e6 = numpy.transpose(_e2) @ _e1
        _e9 = _e3 - lambda_
        _e10 = numpy.sqrt((chi + _e6 @ (_e2)) * _e4)
        result = (
            (psi / chi) ** (lambda_ / 2)
            * _e4 ** _e9
            / (
                (2 * numpy.pi) ** (_e3)
                * numpy.sqrt(n_linalg.det(sigma))
                * s_special.kv(lambda_, numpy.sqrt(psi * chi))
            )
            * numpy.exp(_e6 @ tau)
            * s_special.kv(lambda_ - _e3, _e10)
            / _e10 ** _e9
        )
        return result


def _get_limit_gh_p_probability(x, mu, tau, sigma, lambda_, chi, p):  # aka GH skew t
    _e1 = n_linalg.inv(sigma)
    _e2 = p / 2
    _e3 = x - mu
    _e4 = _e2 - lambda_
    _e7 = numpy.transpose(tau) @ _e1 @ tau
    _e9 = numpy.transpose(_e3) @ _e1
    _e10 = -lambda_
    _e14 = numpy.sqrt((chi + _e9 @ (_e3)) * _e7)
    result = (
        chi ** (_e10)
        * (_e7) ** _e4
        / (
            2 ** (_e4 - 1)
            * numpy.pi ** (_e2)
            * s_special.gamma(_e10)
            * numpy.sqrt(n_linalg.det(sigma))
        )
        * numpy.exp(_e9 @ tau)
        * s_special.kv(lambda_ - _e2, _e14)
        / _e14 ** _e4
    )
    return result


def _get_conditional_limit_gh_p_probability(
    x,
    x1,
    mu,
    mu1,
    tau,
    tau1,
    sigma,
    sigma11,
    lambda_,
    chi,
    p,
    q,
):  # x2 | x1
    _e1 = n_linalg.inv(sigma)
    _e2 = n_linalg.inv(sigma11)
    _e3 = p / 2
    _e4 = q / 2
    _e5 = x - mu
    _e6 = x1 - mu1
    _e7 = -lambda_
    _e8 = _e3 - lambda_
    _e9 = _e4 - lambda_
    _e12 = numpy.transpose(tau) @ _e1 @ tau
    _e15 = numpy.transpose(tau1) @ _e2 @ tau1
    _e17 = numpy.transpose(_e5) @ _e1
    _e19 = numpy.transpose(_e6) @ _e2
    _e25 = _e8
    _e26 = _e9
    _e27 = chi ** (_e7)
    _e28 = s_special.gamma(_e7)
    _e29 = numpy.sqrt((chi + _e17 @ (_e5)) * _e12)
    _e30 = numpy.sqrt((chi + _e19 @ (_e6)) * _e15)
    result = (
        _e27
        * (_e12) ** _e25
        / (2 ** (_e8 - 1) * numpy.pi ** (_e3) * _e28 * numpy.sqrt(n_linalg.det(sigma)))
        * numpy.exp(_e17 @ tau)
        * s_special.kv(lambda_ - _e3, _e29)
        / _e29 ** _e25
        / (
            _e27
            * (_e15) ** _e26
            / (
                2 ** (_e9 - 1)
                * numpy.pi ** (_e4)
                * _e28
                * numpy.sqrt(n_linalg.det(sigma11))
            )
            * numpy.exp(_e19 @ tau1)
            * s_special.kv(lambda_ - _e4, _e30)
            / _e30 ** _e26
        )
    )

    if False:
        _e1 = n_linalg.inv(sigma11)
        _e2 = sigma21 @ _e1
        _e3 = x1 - mu1
        (_e5,) = x1.shape
        _e8 = sigma22 - _e2 @ sigma12
        _e9 = tau2 - _e2 @ tau1
        _e10 = n_linalg.inv(_e8)
        _e11 = lambda_ - _e5 / 2
        _e13 = psi + numpy.transpose(tau1) @ _e1 @ tau1
        _e14 = x2 - (mu2 + _e2 @ _e3)
        _e15 = (p - _e5) / 2
        _e16 = chi + numpy.transpose(_e3) @ _e1 @ _e3
        _e17 = _e13 + numpy.transpose(_e9) @ _e10 @ _e9
        _e19 = numpy.transpose(_e14) @ _e10
        _e20 = _e15 - _e11
        _e23 = numpy.sqrt((_e16 + _e19 @ (_e14)) * _e17)
        result = (
            (_e13 / _e16) ** (_e11 / 2)
            * _e17 ** _e20
            / (
                (2 * numpy.pi) ** (_e15)
                * numpy.sqrt(n_linalg.det(_e8))
                * s_special.kv(_e11, numpy.sqrt(_e13 * _e16))
            )
            * numpy.exp(_e19 @ _e9)
            * s_special.kv(_e11 - _e15, _e23)
            / _e23 ** _e20
        )
    return result
