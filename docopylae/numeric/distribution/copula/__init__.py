import docopylae.distribution.copula as dd_copula
import docopylae.numeric as d_numeric
import docopylae.numeric.distribution.base as dnd_base
import numpy


# POINT = dnd_base.POINT
# POINTS = dnd_base.POINTS


def initialize():  # ()~None
    import docopylae.numeric.distribution.copula.base as dndc_base
    import docopylae.numeric.distribution.copula.vine as dndc_vine

    dd_copula.ImplicitCopulaDistribution.set_specialization_type(
        d_numeric, ImplicitCopulaDistribution
    )

    dd_copula.ProductCopulaDistribution.set_specialization_type(
        d_numeric, ProductCopulaDistribution
    )

    dd_copula.ClaytonCopulaDistribution.set_specialization_type(
        d_numeric, ClaytonCopulaDistribution
    )

    dndc_base.initialize()
    dndc_vine.initialize()


class ImplicitCopulaDistribution(dnd_base.ProbabilityDistribution):

    # gradients

    def get_cumulative_probability_gradient_inverses(
        self, gradients, points, random_variable
    ):  # (gradients~[gradient~object], points~POINTS, random_variable~d_distribution.RandomVariable)~POINTS
        result = self.specializable._get_cumulative_probability_gradient_inverses(
            d_numeric, gradients, points, random_variable
        )
        return result


class ProductCopulaDistribution(dnd_base.ProbabilityDistribution):

    # probabilities

    def get_probabilities(
        self, points
    ):  # (points~POINTS)~numpy.ndarray([probability~number])
        result = numpy.ones((len(points),))
        return result


class ClaytonCopulaDistribution(dnd_base.ProbabilityDistribution):
    def get_parameter_values(self, points):  # (points~POINTS)~[parameter value~object]
        result = numpy.array(self.specializable.get_parameter_values(points))
        return result

    def get_probabilities(
        self, points
    ):  # (points~POINTS)~numpy.ndarray([probability~number])
        result = self.specializable._get_probability(
            self.get_parameter_values(points), points
        )
        return result
