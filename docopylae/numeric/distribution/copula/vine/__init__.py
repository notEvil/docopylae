import docopylae.distribution.copula.vine as ddc_vine
import docopylae.numeric as d_numeric
import docopylae.numeric.distribution.base as dnd_base
import numpy.random as n_random


# POINT = dnd_base.POINT
# POINTS = dnd_base.POINTS


def initialize():  # ()~None
    ddc_vine.VineCopulaDistribution.set_specialization_type(
        d_numeric, VineCopulaDistribution
    )
    ddc_vine._VineMarginalDistribution.set_specialization_type(
        d_numeric, _VineMarginalDistribution
    )


class VineCopulaDistribution(dnd_base.ProbabilityDistribution):

    # probabilities

    def get_probabilities(
        self, points
    ):  # (points~POINTS)~numpy.ndarray([probability~number])
        result = self.specializable._get_probabilities(d_numeric, points)
        return result

    # sampling

    def get_samples(self, points):  # (points~POINTS)~POINTS
        result = self.specializable._get_samples(
            d_numeric, lambda size: n_random.uniform(size=size), points
        )
        return result


class _VineMarginalDistribution(dnd_base.ProbabilityDistribution):

    # probabilities

    def get_cumulative_probabilities(
        self, points
    ):  # (points~POINTS)~numpy.ndarray([cumulative probability~number])
        result = self.specializable._get_cumulative_probabilities(d_numeric, points)
        return result

    def get_quantiles(
        self, cumulative_probabilities, points
    ):  # (cumulative_probabilities~numpy.ndarray([cumulative probability~number]), points~POINTS)~POINTS
        result = self.specializable._get_quantiles(
            d_numeric, cumulative_probabilities, points
        )
        return result
