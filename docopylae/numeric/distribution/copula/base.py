import docopylae.distribution.copula.base as ddc_base
import docopylae.numeric as d_numeric
import docopylae.numeric.distribution.base as dnd_base


# POINT = dnd_base.POINT
# POINTS = dnd_base.POINTS


def initialize():  # ()~None
    ddc_base.MetaDistribution.set_specialization_type(d_numeric, MetaDistribution)


class MetaDistribution(dnd_base.ProbabilityDistribution):

    # probabilities

    def get_probabilities(
        self, points
    ):  # (points~POINTS)~numpy.ndarray([probability~number])
        result = self.specializable._get_probabilities(d_numeric, points)
        return result
