import docopylae.distribution.base as dd_base
import docopylae.numeric as d_numeric
import docopylae.python_tools as d_python_tools
import docopylae.simpson as d_simpson
import docopylae.space as d_space
import numpy


# import docopylae.distribution as d_distribution

# POINT = dd_base.POINT
# POINTS = dd_base.POINTS(type=numpy.ndarray([coordinate~object]))


def initialize():
    # dd_base.DensityDistribution.set_specialization_type(d_numeric, DensityDistribution)
    pass


class ProbabilityDistribution(
    dd_base.CompositeDistribution, d_python_tools.Specialization
):
    def get_base_distribution(self):  # ()~dd_base.ProbabilityDistribution
        return self.specializable

    # statistics

    def get_means(self, points):  # (points~POINTS)~POINTS
        result = self._to_numpy(self.specializable.get_means(points))
        return result

    def get_standard_deviations(self, points):  # (points~POINTS)~POINTS
        result = self._to_numpy(self.specializable.get_standard_deviations(points))
        return result

    # probabilities

    def get_probabilities(
        self, points
    ):  # (points~POINTS)~numpy.ndarray([probability~object])
        result = numpy.array(self.specializable.get_probabilities(points))
        return result

    def get_cumulative_probabilities(
        self, points
    ):  # (points~POINTS)~numpy.ndarray([cumulative probability~number])
        result = numpy.array(self.specializable.get_cumulative_probabilities(points))
        return result

    def get_quantiles(
        self, cumulative_probabilities, points
    ):  # (cumulative_probabilities~numpy.ndarray([cumulative probability~object]), points~POINTS)~POINTS
        result = self._to_numpy(
            self.specializable.get_quantiles(cumulative_probabilities, points)
        )
        return result

    # sampling

    def get_samples(self, points):  # (points~POINTS)~POINTS
        result = self._to_numpy(self.specializable.get_samples(points))
        return result

    # gradients

    def get_cumulative_probability_gradients(
        self, points, random_variable
    ):  # (points~POINTS, random_variable~d_distribution.RandomVariable)~numpy.ndarray([gradient~number])
        result = numpy.array(
            self.specializable.get_cumulative_probability_gradients(
                points, random_variable
            )
        )
        return result

    def get_cumulative_probability_gradient_inverses(
        self, gradients, points, random_variable
    ):  # (gradients~numpy.ndarray([gradient~number]), points~POINTS, random_variable~d_distribution.RandomVariable)~POINTS
        result = self._to_numpy(
            self.specializable.get_cumulative_probability_gradient_inverses(
                gradients, points, random_variable
            )
        )
        return result

    #

    def _to_numpy(self, points):
        points.coordinates.update(
            (dimension, numpy.array(coordinates))
            for dimension, coordinates in points.coordinates.items()
        )
        return points


if False:

    class DensityDistribution(ProbabilityDistribution):

        # probabilities

        def get_quantiles(
            self, cumulative_probabilities, points
        ):  # (cumulative_probabilities~[cumulative probability~object], points~POINTS)~POINTS
            (random_variable,) = self.get_random_variables()

            (means,) = self.get_means(points).coordinates.values()
            (standard_deviations,) = self.get_standard_deviations(
                points
            ).coordinates.values()

            approximate_integral = d_simpson.InfiniteApproximateIntegral(
                -float("inf"),
                mean - 3 * standard_deviation,
                mean + 3 * standard_deviation,
                float("inf"),
                lambda argument: self.get_probability(
                    d_space.Point({random_variable: argument}), parameter_point
                ),
            )
            result = d_space.Points(
                {
                    random_variable: approximate_integral.evaluate_inverses(
                        cumulative_probabilities
                    )
                },
                length=len(points),
            )
            return result
