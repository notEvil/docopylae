import docopylae.base as d_base
import docopylae.python_tools as d_python_tools


class Criterion(d_base.CompositeCriterion, d_python_tools.Specialization):
    def get_base_criterion(self):  # ()~d_base.Criterion
        return self.specializable


class Sensitivity(d_base.CompositeSensitivity, d_python_tools.Specialization):
    def get_base_sensitivity(self):  # ()~d_base.Sensitivity
        return self.specializable
