import docopylae.space as d_space
import numpy
import collections
import collections.abc as c_abc
import operator


class NONE:
    pass


def get_sum(
    iterable, inplace=True, default=0
):  # (iterable~iter(object~object), inplace~bool, default~object)~object
    result = _get_aggregate(
        operator.add, operator.iadd, _sum_shortcuts, iterable, inplace, default
    )
    return result


_sum_shortcuts = [
    (numpy.ndarray, lambda array: array.sum(axis=0)),
    ((d_space.Points, d_space.Matrices), lambda iterable: iterable.get_sum()),
]


def get_product(
    iterable, inplace=True, default=1
):  # (iterable~iter(object~object), inplace~bool, default~object)~object
    result = _get_aggregate(
        operator.mul, operator.imul, _product_shortcuts, iterable, inplace, default
    )
    return result


_product_shortcuts = [
    (numpy.ndarray, lambda array: array.prod(axis=0)),
    ((d_space.Points, d_space.Matrices), lambda iterable: iterable.get_product()),
]


def get_all(
    iterable, inplace=True, default=1
):  # (iterable~iter(object~object), inplace~bool, default~object)~object
    result = _get_aggregate(
        operator.and_, operator.iand, [], iterable, inplace, default
    )
    return result


def _get_aggregate(operation, inplace_operation, shortcuts, iterable, inplace, default):
    for base_types, shortcut in shortcuts:
        if isinstance(iterable, base_types):
            result = shortcut(iterable)
            return result

    iterator = iter(iterable)

    try:
        result = next(iterator)

    except StopIteration:
        return default

    if not inplace:
        for object in iterator:
            result = operation(result, object)

        return result

    try:
        object = next(iterator)

    except StopIteration:
        return result

    result = operation(result, object)
    for object in iterator:
        result = inplace_operation(result, object)

    return result


def get_sequence(object):  # (object~object)~[object~object]
    if isinstance(object, c_abc.Sequence):
        return object

    result = list(object)
    return result


def get_set(object):  # (object~object)~{object~object}
    if isinstance(object, c_abc.Set):
        return object

    result = set(object)
    return result


def unite_sets(
    sets,
):  # (sets~iter(objects~iter(hashable object~hash(object))))~{object~object}
    result = set()
    for objects in sets:
        result.update(objects)
    return result


def set_attributes(
    object, **kwargs
):  # (object~object, **kwargs~{attribute name~str: object~object})~object
    for attribute_name, attribute_object in kwargs.items():
        setattr(object, attribute_name, attribute_object)
    return object


def get_others(
    object, iterable
):  # (object~object, iterable~iter(object~object))~object or None
    for other_object in iterable:
        if other_object != object:
            yield other_object


if False:

    class Extendable:
        _extension_types = collections.ChainMap()

        def __init_subclass__(cls):
            super().__init_subclass__()

            for base_type in cls.__bases__:
                if issubclass(base_type, Extendable):
                    break

            cls._extension_types = base_type._extension_types.new_child()

        def __init__(self):
            super().__init__()

            self._extensions = {}

        @classmethod
        def set_extension_type(
            cls, id, type
        ):  # (id~hash(object), type~(object~Extendable)~object)~None
            cls._extension_types[id] = type

        def get_extension(self, id):  # (id~hash(object))~object
            result = self._extensions.get(id, None)
            if result is not None:
                return result

            result = type(self)._extension_types[id](self)
            self._extensions[id] = result
            return result


class Specializable:
    _specialization_types = collections.ChainMap()

    def __init_subclass__(cls):
        super().__init_subclass__()

        for base_type in cls.__bases__:
            if issubclass(base_type, Specializable):
                break

        cls._specialization_types = base_type._specialization_types.new_child()

    @classmethod
    def set_specialization_type(
        cls, id, type
    ):  # (id~hash(object), type~type(Specialization))~None
        cls._specialization_types[id] = type

    def get_specialization(
        self, id
    ):  # (id~hash(object) or None)~Specialization or Specializable
        if id is None:
            return self

        specialization_type = type(self)._specialization_types.get(id, None)
        if specialization_type is None:
            return self

        result = specialization_type(self)
        return result


class Specialization:
    def __init__(self, specializable):
        super().__init__()

        self.specializable = specializable

    def get_specialization(
        self, id
    ):  # (id~hash(object) or None)~Specialization or Specializable
        result = self.specializable.get_specialization(id)
        return result
