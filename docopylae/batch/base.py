class BatchInterface:
    @staticmethod
    def get_length(object):  # (object~object)~int
        raise NotImplementedError(type(self))

    @staticmethod
    def get_batch(slice, object):  # (slice~slice, object~object)~object
        raise NotImplementedError(type(self))

    @staticmethod
    def get_fill(length, object):  # (length~int, object~object)~object
        raise NotImplementedError(type(self))

    @staticmethod
    def concatenate_batches(objects):  # (objects~iter(object))~object
        raise NotImplementedError(type(self))
