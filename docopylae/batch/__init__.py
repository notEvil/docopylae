import docopylae.batch.base as db_base
import docopylae.python_tools as d_python_tools
import numpy


class BatchedFunction:
    def __init__(
        self, function, batch_size, argument_batch_interface, result_batch_interface
    ):  # (function~function(object~object, *, **)~object, batch_size~int, argument_batch_interface~db_base.BatchInterface, result_batch_interface~db_base.BatchInterface)~BatchedFunction
        super().__init__()

        self.function = function
        self.batch_size = batch_size
        self.argument_batch_interface = argument_batch_interface
        self.result_batch_interface = result_batch_interface

    def __call__(self, object, *args, **kwargs):
        result = self.result_batch_interface.concatenate_batches(
            _get_results(
                (
                    self.function(batch, *args, **kwargs)
                    for batch in _get_batches(
                        object, self.batch_size, self.argument_batch_interface
                    )
                ),
                self.argument_batch_interface.get_length(object),
                self.result_batch_interface,
            )
        )
        return result


def _get_batches(object, batch_size, batch_interface):
    start_index = 0

    while True:
        batch = batch_interface.get_batch(
            slice(start_index, start_index + batch_size), object
        )
        length = batch_interface.get_length(batch)

        if length == 0:
            break

        if length < batch_size:
            batch = batch_interface.concatenate_batches(
                [batch, batch_interface.get_fill(batch_size - length, object)]
            )

        yield batch

        start_index += length


def _get_results(results, result_length, batch_interface):
    results = iter(results)
    result = previous_result = next(results)

    for result in results:
        yield previous_result
        result_length -= batch_interface.get_length(previous_result)

        previous_result = result

    if batch_interface.get_length(result) != result_length:
        result = batch_interface.get_batch(slice(result_length), result)

    yield result


class NumpyArrayBatchInterface(db_base.BatchInterface):
    @staticmethod
    def get_length(object):  # (object~numpy.ndarray)~int
        result = object.shape[0]
        return result

    @staticmethod
    def get_batch(slice, object):  # (slice~slice, object~numpy.ndarray)~numpy.ndarray
        result = object[slice, ...]
        return result

    @staticmethod
    def get_fill(length, object):  # (length~int, object~numpy.ndarray)~numpy.ndarray
        shape = list(object.shape)
        shape[0] = length
        result = numpy.full_like(object, numpy.nan, shape=shape)
        return result

    @staticmethod
    def concatenate_batches(objects):  # (objects~iter(numpy.ndarray))~numpy.ndarray
        result = numpy.concatenate(d_python_tools.get_sequence(objects), axis=0)
        return result


class ArrayDictBatchInterface(db_base.BatchInterface):
    # ARRAY_DICT = {key~object: value~numpy.ndarray}

    @staticmethod
    def get_length(object):  # (object~ARRAY_DICT)~int
        result = len(next(iter(object.values())))
        return result

    @staticmethod
    def get_batch(slice, object):  # (slice~slice, object~ARRAY_DICT)~ARRAY_DICT
        result = {key: value[slice] for key, value in object.items()}
        return result

    @staticmethod
    def get_fill(length, object):  # (length~int, object~ARRAY_DICT)~ARRAY_DICT
        value = NumpyArrayBatchInterface.get_fill(length, next(iter(object.values())))
        result = {key: value for key in object.keys()}
        return result

    @staticmethod
    def concatenate_batches(objects):  # (objects~iter(ARRAY_DICT))~ARRAY_DICT
        objects = d_python_tools.get_sequence(objects)
        result = {
            key: NumpyArrayBatchInterface.concatenate_batches(
                (object[key] for object in objects)
            )
            for key in objects[0].keys()
        }
        return result
