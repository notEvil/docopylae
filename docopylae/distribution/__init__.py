import docopylae.distribution.base as dd_base
import docopylae.space as d_space
import docopylae.simpson as d_simpson


# import docopylae.distribution as d_distribution

# POINT = dd_base.POINT
# POINTS = dd_base.POINTS


class Parameter(d_space.Dimension):
    def __init__(self, id):  # (id~object)~Parameter
        super().__init__(id)

    def __repr__(self):
        result = "Parameter({})".format(repr(self.id))
        return result


class RandomVariable(d_space.Dimension):
    def __init__(self, id):  # (id~object)~RandomVariable
        super().__init__(id)

    def __repr__(self):
        result = "RandomVariable({})".format(repr(self.id))
        return result


if False:

    class _MarginalDensityDistribution(dd_base.MultivariateDistribution):
        def __init__(
            self,
            random_variables,
            conditional_random_variables,
            multivariate_distribution,
        ):
            super().__init__()

            self.random_variables = random_variables
            self.conditional_random_variables = conditional_random_variables
            self.multivariate_distribution = multivariate_distribution


class UnivariateUniformDistribution(dd_base.UnivariateDistribution):
    def get_begin_value(self, point):  # (point~POINT)~object
        return 0

    def get_begin_values(self, points):  # (points~POINTS)~[begin value~object]
        result = [0] * len(points)
        return result

    def get_end_value(self, point):  # ()~object
        return 1

    def get_end_values(self, points):  # (points~POINTS)~[end value~object]
        result = [1] * len(points)
        return result

    # probabilities

    def get_quantile(
        self, cumulative_probability, point
    ):  # (cumulative_probability~object, point~POINT)~POINT
        (random_variable,) = self.get_random_variables()
        begin_value = self.get_begin_value(point)
        end_value = self.get_end_value(point)
        result = d_space.Point(
            {
                random_variable: cumulative_probability * (end_value - begin_value)
                + begin_value
            }
        )
        return result


class UnivariateNormalDistribution(dd_base.ProbabilityDistribution):
    pass


class GeometricHyperbolicSkewTDistribution(dd_base.MultivariateDistribution):

    # core

    def get_marginal_distribution(
        self, random_variables, conditional_random_variables
    ):  # (random_variables~{random variable~d_distribution.RandomVariable}, conditional_random_variables~{random variable~d_distribution.RandomVariable})~dd_base.ProbabilityDistribution
        result = _MarginalGeometricHyperbolicSkewTDistribution(
            random_variables, conditional_random_variables, self
        )
        return result


class _MarginalGeometricHyperbolicSkewTDistribution(dd_base.MultivariateDistribution):
    def __init__(
        self, random_variables, conditional_random_variables, unconditional_distribution
    ):
        super().__init__()

        self.random_variables = random_variables
        self.conditional_random_variables = conditional_random_variables
        self.unconditional_distribution = unconditional_distribution

    # core

    def get_random_variables(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable}
        return self.random_variables

    def get_conditional_random_variables(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable}
        return self.conditional_random_variables
