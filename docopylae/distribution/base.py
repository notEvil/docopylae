import docopylae.distribution as d_distribution
import docopylae.python_tools as d_python_tools
import docopylae.space as d_space


# import docopylae.distribution.copula.base as ddc_base
# import docopylae.space.base as ds_base

# POINT(type) = d_space.Point(coordinates~{dimension~d_distribution.Parameter | d_distribution.RandomVariable: coordinate~type})
# POINTS(type) = d_space.Points(coordinates~{dimension~d_distribution.Parameter | d_distribution.RandomVariable: coordinates~type})

# POINT = POINTS(type=object)
# POINTS = POINTS(type=[coordinate~object])


class ProbabilityDistribution(d_python_tools.Specializable):
    """
    - represents a parameterized multivariate probability distribution
    - includes random variables for simplicity
    """

    # core

    def get_random_variables(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable}
        raise NotImplementedError(type(self))

    def get_conditional_random_variables(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable}
        raise NotImplementedError(type(self))

    def get_sample_space(self):  # ()~ds_base.Space
        raise NotImplementedError(type(self))

    def get_copula(
        self,
    ):  # ()~(copula distribution~ddc_base.CopulaDistribution, variable map~{random variable~d_distribution.RandomVariable: copula variable~d_distribution.RandomVariable})
        raise NotImplementedError(type(self))

    def get_marginal_distribution(
        self, random_variables, conditional_random_variables
    ):  # (random_variables~{random variable~d_distribution.RandomVariable}, conditional_random_variables~{random variable~d_distribution.RandomVariable})~ProbabilityDistribution
        raise NotImplementedError(type(self))

    # statistics

    def get_mean(self, point):  # (point~POINT)~POINT
        raise NotImplementedError(type(self))

    def get_means(self, points):  # (points~POINTS)~POINTS
        result = d_space.pack_points(self.get_mean(point) for point in points)
        return result

    def get_standard_deviation(self, point):  # (point~POINT)~POINT
        raise NotImplementedError(type(self))

    def get_standard_deviations(self, points):  # (points~POINTS)~POINTS
        result = d_space.pack_points(
            self.get_standard_deviation(point) for point in points
        )
        return result

    # probabilities

    def get_probability(self, point):  # (point~POINT)~object
        raise NotImplementedError(type(self))

    def get_probabilities(self, points):  # (points~POINTS)~[probability~object]
        result = [self.get_probability(point) for point in points]
        return result

    def get_cumulative_probability(self, point):  # (point~POINT)~object
        raise NotImplementedError(type(self))

    def get_cumulative_probabilities(
        self, points
    ):  # (points~POINTS)~[cumulative probability~object]
        result = [self.get_cumulative_probability(point) for point in points]
        return result

    def get_quantile(
        self, cumulative_probability, point
    ):  # (cumulative_probability~object, point~POINT)~POINT
        raise NotImplementedError(type(self))

    def get_quantiles(
        self, cumulative_probabilities, points
    ):  # (cumulative_probabilities~[cumulative probability~object], points~POINTS)~POINTS
        result = d_space.pack_points(
            self.get_quantile(cumulative_probability, point)
            for cumulative_probability, point in zip(cumulative_probabilities, points)
        )
        return result

    # sampling

    def get_sample(self, point):  # (point~POINT)~POINT
        raise NotImplementedError(type(self))

    def get_samples(self, points):  # (points~POINTS)~POINTS
        result = d_space.pack_points(self.get_sample(point) for point in points)
        return result

    # gradients

    def get_cumulative_probability_gradient(
        self, point, random_variable
    ):  # (point~POINT, random_variable~d_distribution.RandomVariable)~object
        raise NotImplementedError(type(self))

    def get_cumulative_probability_gradients(
        self, points, random_variable
    ):  # (points~POINTS, random_variable~d_distribution.RandomVariable)~[gradient~object]
        result = [
            self.get_cumulative_probability_gradient(point, random_variable)
            for point in points
        ]
        return result

    def get_cumulative_probability_gradient_inverse(
        self, gradient, point, random_variable
    ):  # (gradient~object, point~POINT, random_variable~d_distribution.RandomVariable)~POINT
        raise NotImplementedError(type(self))

    def get_cumulative_probability_gradient_inverses(
        self, gradients, points, random_variable
    ):  # (gradients~[gradient~object], points~POINTS, random_variable~d_distribution.RandomVariable)~POINTS
        result = d_space.pack_points(
            self.get_cumulative_probability_gradient_inverse(
                gradient, point, random_variable
            )
            for gradient, point in zip(gradients, points)
        )
        return result


class CompositeDistribution(ProbabilityDistribution):
    def get_base_distribution(self):  # ()~ProbabilityDistribution
        raise NotImplementedError(type(self))

    # core

    def get_random_variables(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable}
        distribution = self.get_base_distribution()
        result = distribution.get_random_variables()
        return result

    def get_conditional_random_variables(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable}
        distribution = self.get_base_distribution()
        result = distribution.get_conditional_random_variables()
        return result

    def get_sample_space(self):  # ()~ds_base.Space
        distribution = self.get_base_distribution()
        result = distribution.get_sample_space()
        return result

    def get_copula(
        self,
    ):  # ()~(copula distribution~ddc_base.CopulaDistribution, variable map~{random variable~d_distribution.RandomVariable: copula variable~d_distribution.RandomVariable})
        distribution = self.get_base_distribution()
        result = distribution.get_copula()
        return result

    def get_marginal_distribution(
        self, random_variables, conditional_random_variables
    ):  # (random_variables~{random variable~d_distribution.RandomVariable}, conditional_random_variables~{random variable~d_distribution.RandomVariable})~ProbabilityDistribution
        distribution = self.get_base_distribution()
        result = distribution.get_marginal_distribution(
            random_variables, conditional_random_variables
        )
        return result

    # statistics

    def get_mean(self, point):  # (point~POINT)~POINT
        distribution = self.get_base_distribution()
        result = distribution.get_mean(point)
        return result

    def get_means(self, points):  # (points~POINTS)~POINTS
        distribution = self.get_base_distribution()
        result = distribution.get_means(points)
        return result

    def get_standard_deviation(self, point):  # (point~POINT)~POINT
        distribution = self.get_base_distribution()
        result = distribution.get_standard_deviation(point)
        return result

    def get_standard_deviations(self, points):  # (points~POINTS)~POINTS
        distribution = self.get_base_distribution()
        result = distribution.get_standard_deviations(points)
        return result

    # probabilities

    def get_probability(self, point):  # (point~POINT)~object
        distribution = self.get_base_distribution()
        result = distribution.get_probability(point)
        return result

    def get_probabilities(self, points):  # (points~POINTS)~[probability~object]
        distribution = self.get_base_distribution()
        result = distribution.get_probabilities(points)
        return result

    def get_cumulative_probability(self, point):  # (point~POINT)~object
        distribution = self.get_base_distribution()
        result = distribution.get_cumulative_probability(point)
        return result

    def get_cumulative_probabilities(
        self, points
    ):  # (points~POINTS)~[cumulative probability~object]
        distribution = self.get_base_distribution()
        result = distribution.get_cumulative_probabilities(points)
        return result

    def get_quantile(
        self, cumulative_probability, point
    ):  # (cumulative_probability~object, point~POINT)~POINT
        distribution = self.get_base_distribution()
        result = distribution.get_quantile(cumulative_probability, point)
        return result

    def get_quantiles(
        self, cumulative_probabilities, points
    ):  # (cumulative_probabilities~[cumulative probability~object], points~POINTS)~POINTS
        distribution = self.get_base_distribution()
        result = distribution.get_quantiles(cumulative_probabilities, points)
        return result

    # sampling

    def get_sample(self, point):  # (point~POINT)~POINT
        distribution = self.get_base_distribution()
        result = distribution.get_sample(point)
        return result

    def get_samples(self, points):  # (points~POINTS)~POINTS
        distribution = self.get_base_distribution()
        result = distribution.get_samples(points)
        return result

    # gradients

    def get_cumulative_probability_gradient(
        self, point, random_variable
    ):  # (point~POINT, random_variable~d_distribution.RandomVariable)~object
        distribution = self.get_base_distribution()
        result = distribution.get_cumulative_probability_gradient(
            point, random_variable
        )
        return result

    def get_cumulative_probability_gradients(
        self, points, random_variable
    ):  # (points~POINTS, random_variable~d_distribution.RandomVariable)~[gradient~object]
        distribution = self.get_base_distribution()
        result = distribution.get_cumulative_probability_gradients(
            points, random_variable
        )
        return result

    def get_cumulative_probability_gradient_inverse(
        self, gradient, point, random_variable
    ):  # (gradient~object, point~POINT, random_variable~d_distribution.RandomVariable)~POINT
        distribution = self.get_base_distribution()
        result = distribution.get_cumulative_probability_gradient_inverse(
            gradient, point, random_variable
        )
        return result

    def get_cumulative_probability_gradient_inverses(
        self, gradients, points, random_variable
    ):  # (gradients~[gradient~object], points~POINTS, random_variable~d_distribution.RandomVariable)~POINTS
        distribution = self.get_base_distribution()
        result = distribution.get_cumulative_probability_gradient_inverses(
            gradients, points, random_variable
        )
        return result


class UnivariateDistribution(ProbabilityDistribution):  # TODO useful?

    # core

    def get_copula(
        self,
    ):  # ()~(copula distribution~ddc_base.CopulaDistribution, variable map~{random variable~d_distribution.RandomVariable: copula variable~d_distribution.RandomVariable})
        raise TypeError(type(self))

    def get_marginal_distribution(
        self, random_variables, conditional_random_variables
    ):  # (random_variables~{random variable~d_distribution.RandomVariable}, conditional_random_variables~{random variable~d_distribution.RandomVariable})~ProbabilityDistribution
        raise TypeError(type(self))


class MultivariateDistribution(ProbabilityDistribution):
    def __init__(self):  # ()~MultivariateDistribution
        super().__init__()

        self._copula_distribution = None

    # core

    def get_copula(
        self,
    ):  # ()~(copula distribution~ddc_base.CopulaDistribution, variable map~{random variable~d_distribution.RandomVariable: copula variable~d_distribution.RandomVariable})
        import docopylae.distribution.copula as dd_copula  # TODO resolve circular dependency

        if self._copula_distribution is None:
            self._copula_distribution = dd_copula.ImplicitCopulaDistribution(self)

        variable_map = self._copula_distribution.get_variable_map()
        if len(variable_map) == 1:
            raise TypeError(type(self))

        result = (self._copula_distribution, variable_map)
        return result


if False:  # TODO solve problem related to implicit integrals

    class DensityDistribution(MultivariateDistribution):

        # core

        def get_marginal_distribution(
            self, random_variables, conditional_random_variables
        ):  # (random_variables~{random variable~d_distribution.RandomVariable}, conditional_random_variables~{random variable~d_distribution.RandomVariable})~ProbabilityDistribution
            result = d_distribution._MarginalDensityDistribution(
                random_variables, conditional_random_variables, self
            )
            return result
