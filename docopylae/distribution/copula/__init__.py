import docopylae.distribution as d_distribution
import docopylae.distribution.copula.base as ddc_base
import docopylae.python_tools as d_python_tools
import docopylae.space as d_space
import uuid


# import docopylae.distribution.base as dd_base

# POINT = dd_base.POINT
# POINTS = dd_base.POINTS


class ImplicitCopulaDistribution(ddc_base.CopulaDistribution):
    def __init__(
        self, probability_distribution
    ):  # (probability_distribution~dd_base.ProbabilityDistribution)~ImplicitCopulaDistribution
        super().__init__()

        self.probability_distribution = probability_distribution

        self._variable_map = None
        self._inverse_map = None

    def get_variable_map(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable: copula variable~d_distribution.RandomVariable}
        if self._variable_map is not None:
            return self._variable_map

        self._variable_map = {
            random_variable: d_distribution.RandomVariable(uuid.uuid4())
            for random_variable in self.probability_distribution.get_random_variables()
        }
        self._inverse_map = {
            copula_variable: random_variable
            for random_variable, copula_variable in self._variable_map.items()
        }
        return self._variable_map

    # core

    def get_random_variables(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable}
        result = set(self.get_variable_map().values())
        return result

    def get_conditional_random_variables(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable}
        result = self.probability_distribution.get_conditional_random_variables()
        return result

    # gradients

    def get_cumulative_probability_gradient_inverses(
        self, gradients, points, random_variable
    ):  # (gradients~[gradient~object], points~POINTS, random_variable~d_distribution.RandomVariable)~POINTS
        result = self._get_cumulative_probability_gradient_inverses(
            None, gradients, points, random_variable
        )
        return result

    def _get_cumulative_probability_gradient_inverses(
        self, specialization_id, gradients, points, random_variable
    ):
        self.get_variable_map()

        if False:
            print(random_variable)
            print(self.get_random_variables())
        (other_random_variable,) = d_python_tools.get_others(
            random_variable, self.get_random_variables()
        )
        conditional_random_variables = self.get_conditional_random_variables()

        meta_random_variable = self._inverse_map[random_variable]
        other_meta_random_variable = self._inverse_map[other_random_variable]

        meta_marginal_distribution = (
            self.probability_distribution.get_marginal_distribution(
                {meta_random_variable}, conditional_random_variables
            )
        )
        other_meta_marginal_distribution = (
            self.probability_distribution.get_marginal_distribution(
                {other_meta_random_variable}, conditional_random_variables
            )
        )
        meta_conditional_marginal_distribution = (
            self.probability_distribution.get_marginal_distribution(
                {other_meta_random_variable},
                conditional_random_variables | {meta_random_variable},
            )
        )

        result = meta_conditional_marginal_distribution.get_specialization(
            specialization_id
        ).get_cumulative_probabilities(
            points
            & other_meta_marginal_distribution.get_specialization(
                specialization_id
            ).get_quantiles(gradients, points)
            & meta_marginal_distribution.get_specializaton(
                specialization_id
            ).get_quantiles(points[random_variable], points)
        )
        return result


class ProductCopulaDistribution(ddc_base.CopulaDistribution):

    # probabilities

    def get_probability(self, point):  # (point~POINT)~object
        return 1

    # gradients

    def get_cumulative_probability_gradients(
        self, points, random_variable
    ):  # (points~POINTS, random_variable~d_distribution.RandomVariable)~[gradient~object]
        if False:
            print(random_variable)
            print(self.get_random_variables())
        (other_random_variable,) = d_python_tools.get_others(
            random_variable, self.get_random_variables()
        )
        result = points[other_random_variable]
        return result

    def get_cumulative_probability_gradient_inverses(
        self, gradients, points, random_variable
    ):  # (gradients~[gradient~object], points~POINTS, random_variable~d_distribution.RandomVariable)~POINTS
        (other_random_variable,) = d_python_tools.get_others(
            random_variable, self.get_random_variables()
        )
        result = d_space.Points({other_random_variable: gradients}, length=len(points))
        return result


class ClaytonCopulaDistribution(ddc_base.CopulaDistribution):
    def get_parameter_value(self, point):  # (point~POINT)~object
        raise NotImplementedError(type(self))

    def get_parameter_values(self, points):  # (points~POINTS)~[parameter value~object]
        result = [self.get_parameter_value(point) for point in points]
        return result

    # probabilities

    def get_probability(self, point):  # (point~POINT)~object
        result = self._get_probability(self.get_parameter_value(point), point)
        return result

    def _get_probability(self, alpha, point):
        u1, u2 = (
            point[random_variable] for random_variable in self.get_random_variables()
        )
        result = (
            (1 + (u1 ** (-alpha) - 1 + u2 ** (-alpha) - 1)) ** (((-1 / alpha) - 1) - 1)
            * (((-1 / alpha) - 1) * (u2 ** ((-alpha) - 1) * (-alpha)))
            * ((-1 / alpha) * (u1 ** ((-alpha) - 1) * (-alpha)))
        )
        return result

    # gradients

    def get_cumulative_probability_gradients(
        self, points, random_variable
    ):  # (points~POINTS, random_variable~d_distribution.RandomVariable)~[gradient~object]
        (other_random_variable,) = d_python_tools.get_others(
            random_variable, self.get_random_variables()
        )
        u1 = points[random_variable]
        u2 = points[other_random_variable]
        alpha = self.get_parameter_values(points)
        result = u1 ** (-1 - alpha) / (1 / u1 ** alpha + 1 / u2 ** alpha - 1) ** (
            1 + 1 / alpha
        )
        return result

    def get_cumulative_probability_gradient_inverses(
        self, gradients, points, random_variable
    ):  # (gradients~[gradient~object], points~POINTS, random_variable~d_distribution.RandomVariable)~POINTS
        u = points[random_variable]
        v = gradients
        alpha = self.get_parameter_values(points)
        _e1 = 1 + alpha
        result = d_space.Points(
            {
                random_variable: ((u ** _e1 * v) ** (1 / _e1 - 1) + 1 - 1 / u ** alpha)
                ** -(1 / alpha)
            },
            length=len(points),
        )
        return result
