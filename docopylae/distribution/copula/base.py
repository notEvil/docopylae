import docopylae.distribution as d_distribution
import docopylae.distribution.base as dd_base
import docopylae.distribution.copula.vine as ddc_vine
import docopylae.space as d_space
import docopylae.python_tools as d_python_tools


# POINT = dd_base.POINT
# POINTS = dd_base.POINTS


class CopulaDistribution(dd_base.ProbabilityDistribution):
    def __init__(self):  # ()~CopulaDistribution
        super().__init__()

        self._marginal_distributions = {}
        self._conditional_marginal_distributions = {}

    # core

    def get_copula(
        self,
    ):  # ()~(copula distribution~CopulaDistribution, variable map~{random variable~d_distribution.RandomVariable: copula variable~d_distribution.RandomVariable})
        result = (
            self,
            {
                random_variable: random_variable
                for random_variable in self.get_random_variables()
            },
        )
        return result

    def get_marginal_distribution(
        self, random_variables, conditional_random_variables
    ):  # (random_variables~{random variable~d_distribution.RandomVariable}, conditional_random_variables~{random variable~d_distribution.RandomVariable})~dd_base.ProbabilityDistribution
        self_random_variables = self.get_random_variables()

        if len(self_random_variables) == 2:
            (random_variable,) = random_variables

            assert random_variable in self_random_variables

            self_conditional_random_variables = self.get_conditional_random_variables()

            if conditional_random_variables == self_conditional_random_variables:
                result = self._marginal_distributions.get(random_variable, None)
                if result is not None:
                    return result

                result = _UnivariateUniformDistribution(
                    {random_variable}, conditional_random_variables
                )
                self._marginal_distributions[random_variable] = result
                return result

            (other_random_variable,) = d_python_tools.get_others(
                random_variable, self_random_variables
            )
            conditional_random_variables = set(conditional_random_variables)
            conditional_random_variables.remove(other_random_variable)

            if conditional_random_variables == self_conditional_random_variables:
                result = self._conditional_marginal_distributions.get(
                    random_variable, None
                )
                if result is not None:
                    return result

                result = ddc_vine._VineMarginalDistribution(
                    self, other_random_variable
                )  # TODO generalize VineMarginalDistribution
                self._conditional_marginal_distributions[random_variable] = result
                return result

        raise NotImplementedError(type(self))


class _UnivariateUniformDistribution(d_distribution.UnivariateUniformDistribution):
    def __init__(self, random_variables, conditional_random_variables):
        super().__init__()

        self.random_variables = random_variables
        self.conditional_random_variables = conditional_random_variables

    def get_random_variables(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable}
        return self.random_variables

    def get_conditional_random_variables(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable}
        return self.conditional_random_variables


class MetaDistribution(dd_base.ProbabilityDistribution):

    # probabilities

    def get_probability(self, point):  # (point~POINT)~object
        result = self._get_probability(None, point)
        return result

    def _get_probability(self, specialization_id, point):
        copula_distribution, copula_variable_map = self.get_copula()
        copula_distribution = copula_distribution.get_specialization(specialization_id)

        conditional_random_variables = self.get_conditional_random_variables()
        marginal_distributions = {
            random_variable: self.get_marginal_distribution(
                {random_variable}, conditional_random_variables
            ).get_specialization(specialization_id)
            for random_variable in self.get_random_variables()
        }

        point = point & d_space.Point(
            {
                copula_variable_map[
                    random_variable
                ]: marginal_distribution.get_cumulative_probability(point)
                for random_variable, marginal_distribution in marginal_distributions.items()
            }
        )
        result = copula_distribution.get_probability(
            point
        ) * d_python_tools.get_product(
            marginal_distribution.get_probability(point)
            for marginal_distribution in marginal_distributions.values()
        )
        return result

    def get_probabilities(self, points):  # (points~POINTS)~[probability~object]
        result = self._get_probabilities(None, points)
        return result

    def _get_probabilities(
        self, specialization_id, points
    ):  # TODO merge with _get_probability
        copula_distribution, copula_variable_map = self.get_copula()
        copula_distribution = copula_distribution.get_specialization(specialization_id)

        conditional_random_variables = self.get_conditional_random_variables()
        marginal_distributions = {
            random_variable: self.get_marginal_distribution(
                {random_variable}, conditional_random_variables
            ).get_specialization(specialization_id)
            for random_variable in self.get_random_variables()
        }

        points = points & d_space.Points(
            {
                copula_variable_map[
                    random_variable
                ]: marginal_distribution.get_cumulative_probabilities(points)
                for random_variable, marginal_distribution in marginal_distributions.items()
            },
            length=len(points),
        )
        result = copula_distribution.get_probabilities(
            points
        ) * d_python_tools.get_product(
            marginal_distribution.get_probabilities(points)
            for marginal_distribution in marginal_distributions.values()
        )
        return result
