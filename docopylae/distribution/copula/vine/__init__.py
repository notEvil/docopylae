import docopylae.distribution as d_distribution
import docopylae.distribution.base as dd_base
import docopylae.distribution.copula.vine._graph_tools as ddcv_graph_tools
import docopylae.python_tools as d_python_tools
import docopylae.space as d_space
import itertools
import random


# import docopylae.distribution.copula.base as ddc_base

# POINT = dd_base.POINT
# POINTS = dd_base.POINTS


class VineCopulaDistribution(dd_base.ProbabilityDistribution):
    def __init__(self):  # ()~VineCopulaDistribution
        super().__init__()

        self._distribution_matrix = None
        self._random_variables = None

    def get_copula_matrix(
        self,
    ):  # ()~[column~[copula~(copula distribution~ddc_base.CopulaDistribution, variable map~{random variable~d_distribution.RandomVariable: copula variable~d_distribution.RandomVariable})]]
        raise NotImplementedError(type(self))

    def _get_distribution_matrix(self):
        if self._distribution_matrix is not None:
            return self._distribution_matrix

        self._distribution_matrix = _get_distribution_matrix(self.get_copula_matrix())
        return self._distribution_matrix

    # core

    def get_random_variables(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable}
        if self._random_variables is not None:
            return self._random_variables

        result = set()
        for column_index, column in enumerate(self._get_distribution_matrix()):
            (random_variable,) = column[column_index].get_random_variables()
            result.add(random_variable)

        self._random_variables = result
        return result

    if False:  # TODO needs thinking

        def get_marginal_distribution(
            self, random_variables, conditional_random_variables
        ):  # (random_variables~{random variable~d_distribution.RandomVariable}, conditional_random_variables~{random variable~d_distribution.RandomVariable})~dd_base.ProbabilityDistribution
            distribution_matrix = self._get_distribution_matrix()

            if len(random_variables) == 1 and len(conditional_random_variables) == 0:
                (random_variable,) = random_variables

                for column_index, column in enumerate(distribution_matrix):
                    distribution = column[column_index]
                    (candidate_random_variable,) = distribution.get_random_variables()
                    if candidate_random_variable == random_variable:
                        return distribution

            if len(random_variables) == 2:
                for column in distribution_matrix[
                    (len(conditional_random_variables) + 1) :
                ]:
                    distribution = column[len(conditional_random_variables)]

                    if (
                        distribution.get_random_variables() == random_variables
                        and distribution.get_conditional_random_variables()
                        == conditional_random_variables
                    ):
                        return distribution

            raise NotImplementedError(
                (random_variables, conditional_random_variables)
            )  # TODO might also be invalid

    if False:  # not entirely correct

        def get_copula(
            self,
        ):  # ()~(d_base.ProbabilityDistribution, {random variable~RandomVariable: random variable~RandomVariable})
            distribution_matrix = self._get_distribution_matrix()

            result_distribution_matrix = []
            result_random_variables = {}

            for column_index, column in enumerate(distribution_matrix):
                distributions = column[:column_index]

                result_random_variable = RandomVariable(id=None)
                result_random_variable.id = id(result_random_variable)

                distributions.append(
                    set_attributes(
                        UnivariateUniformDistribution(),
                        get_random_variables=lambda random_variable=result_random_variable: {
                            random_variable
                        },
                    )
                )

                (random_variable,) = column[column_index].get_random_variables()
                result_random_variables[random_variable] = result_random_variable

            result_distribution = set_attributes(
                VineCopulaDistribution(),
                get_random_variables=lambda: result_random_variables.values(),
                get_distribution_matrix=lambda: result_distribution_matrix,
            )

            result = (result_distribution, result_random_variables)
            return result

    # probabilities

    def get_probabilities(self, points):  # (points~POINTS)~[probability~object]
        result = self._get_probabilities(None, points)
        return result

    def _get_probabilities(
        self, specialization_id, points
    ):  # TODO cache results, compute in layers
        distribution_matrix = self._get_distribution_matrix()

        points = points.copy()

        def _():
            for column_index, column in enumerate(distribution_matrix):
                for pair_distribution in column[:column_index]:
                    conditional_random_variables = (
                        pair_distribution.get_conditional_random_variables()
                    )
                    copula_distribution, variable_map = pair_distribution.get_copula()

                    marginal_distributions = (
                        (
                            variable_map[random_variable],
                            pair_distribution.get_marginal_distribution(
                                {random_variable}, conditional_random_variables
                            ).get_specialization(specialization_id),
                        )
                        for random_variable in pair_distribution.get_random_variables()
                    )
                    points &= d_space.Points(
                        {
                            copula_random_variable: marginal_distribution.get_cumulative_probabilities(
                                points
                            )
                            for copula_random_variable, marginal_distribution in marginal_distributions
                        },
                        length=len(points),
                    )

                    sub_result = copula_distribution.get_probabilities(points)
                    yield sub_result

        result = d_python_tools.get_product(_())
        return result

    # sampling

    def get_samples(self, points):  # (points~POINTS)~POINTS
        result = self._get_samples(
            None, lambda size: [random.random() for _ in range(size)], points
        )
        return result

    def _get_samples(self, specialization_id, get_random_uniforms, points):
        distribution_matrix = self._get_distribution_matrix()

        cumulative_probabilities = iter(
            get_random_uniforms(len(points)) for _ in itertools.count()
        )

        if False:
            import pandas

            cumulative_probabilities = iter(
                numpy.transpose(pandas.read_csv("~/t.csv").values)
            )

        if False:  # use only last column
            column_index = len(distribution_matrix) - 1
            column = distribution_matrix[column_index]

            column_distribution = column[column_index].get_specialization(
                specialization_id
            )
            result = points & column_distribution.get_quantiles(
                next(cumulative_probabilities), points
            )

            (column_random_variable,) = column_distribution.get_random_variables()
            for distribution in column[:column_index]:
                other_random_variables = d_python_tools.get_others(
                    column_random_variable, distribution.get_random_variables()
                )
                sub_result = (
                    distribution.get_marginal_distribution(
                        {other_random_variable}, result.coordinates.keys()
                    )
                    .get_specialization(specialization_id)
                    .get_quantiles(next(cumulative_probabilities), result)
                )
                result &= sub_result

        if True:  # use second diagonal (like VineCopula::RVineSim in R)

            result = (
                distribution_matrix[0][0]
                .get_specialization(specialization_id)
                .get_quantiles(next(cumulative_probabilities), points)
            )

            for column_index, column in zip(
                itertools.count(1), distribution_matrix[1:]
            ):
                (random_variable,) = column[column_index].get_random_variables()
                pair_distribution = column[column_index - 1]
                sub_result = (
                    pair_distribution.get_marginal_distribution(
                        {random_variable}, result.coordinates.keys()
                    )
                    .get_specialization(specialization_id)
                    .get_quantiles(next(cumulative_probabilities), points & result)
                )
                result &= sub_result

        return result


def _get_distribution_matrix(copula_matrix):
    # create id matrix

    _, variable_map = copula_matrix[1][0]
    random_variable_1, random_variable_2 = variable_map.keys()

    id_matrix = [[random_variable_1], [random_variable_1, random_variable_2]]

    for column_index, column in zip(itertools.count(2), copula_matrix[2:]):
        _, variable_map_0 = column[0]
        _, variable_map_1 = column[1]
        (column_random_variable,) = variable_map_0.keys() & variable_map_1.keys()

        id_column = []

        for _, variable_map in column[:column_index]:
            (random_variable,) = d_python_tools.get_others(
                column_random_variable, variable_map.keys()
            )
            id_column.append(random_variable)

        id_column.append(column_random_variable)

        id_matrix.append(id_column)

    #

    node_matrix = ddcv_graph_tools.get_node_matrix(id_matrix)

    copulas = {
        (
            frozenset(variable_map.keys()),
            frozenset(copula_distribution.get_conditional_random_variables()),
        ): (copula_distribution, variable_map)
        for column_index, column in enumerate(copula_matrix)
        for copula_distribution, variable_map in column[:column_index]
    }

    distribution_cache = {
        margin_node: _UnivariateUniformDistribution(margin_node.id)
        for margin_node in (
            column[column_index] for column_index, column in enumerate(node_matrix)
        )
    }
    result = []

    for column_index, column in enumerate(node_matrix):
        result_column = []

        for pair_node in column[:column_index]:
            pair_distribution = _get_pair_distribution(
                pair_node, copulas, distribution_cache
            )

            result_column.append(pair_distribution)

        marginal_distribution = _get_marginal_distribution(
            column[column_index], None, distribution_cache
        )
        result_column.append(marginal_distribution)

        result.append(result_column)

    return result


class _UnivariateUniformDistribution(d_distribution.UnivariateUniformDistribution):
    def __init__(self, random_variable):
        super().__init__()

        self.random_variable = random_variable

    def get_random_variables(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable}
        result = {self.random_variable}
        return result

    def get_conditional_random_variables(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable}
        result = set()
        return result


def _get_pair_distribution(pair_node, copulas, cache):
    cache_key = pair_node

    result = cache.get(cache_key, None)
    if result is not None:
        return result

    copula_distribution, copula_variable_map = copulas[
        (frozenset(pair_node.ids), frozenset(pair_node.conditional_ids))
    ]

    marginal_distributions = {
        random_variable: _get_marginal_distribution(margin_node, copulas, cache)
        for random_variable, margin_node in pair_node.margin_nodes.items()
    }

    result = _VinePairDistribution(
        copula_distribution, copula_variable_map, marginal_distributions
    )

    cache[cache_key] = result
    return result


def _get_marginal_distribution(margin_node, copulas, cache):
    cache_key = margin_node

    result = cache.get(cache_key, None)
    if result is not None:
        return result

    vine_pair_distribution = _get_pair_distribution(
        margin_node.pair_node, copulas, cache
    )
    random_variable = margin_node.id
    (other_random_variable,) = d_python_tools.get_others(
        random_variable, margin_node.pair_node.ids
    )
    result = _VineMarginalDistribution(vine_pair_distribution, other_random_variable)

    vine_pair_distribution._conditional_marginal_distributions[random_variable] = result

    cache[cache_key] = result
    return result


class _VinePairDistribution(
    dd_base.ProbabilityDistribution
):  # TODO subclass ddc_base.MetaDistribution and use it like one
    def __init__(
        self, copula_distribution, copula_variable_map, marginal_distributions
    ):  # (copula_distribution~dd_base.ProbabilityDistribution, copula_variable_map~{random variable~d_distribution.RandomVariable: copula variable~d_distribution.RandomVariable}, marginal_distributions~{random variable~d_distribution.RandomVariable: distribution~dd_base.ProbabilityDistribution})~_VinePairDistribution
        super().__init__()

        self.copula_distribution = copula_distribution
        self.copula_variable_map = copula_variable_map
        self.marginal_distributions = marginal_distributions

        self._conditional_marginal_distributions = {}

    # core

    def get_random_variables(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable}
        result = set(self.copula_variable_map.keys())
        return result

    def get_conditional_random_variables(
        self,
    ):  # ()~{random variable~d_distribution.RandomVariable}
        result = self.copula_distribution.get_conditional_random_variables()
        return result

    def get_marginal_distribution(
        self, random_variables, conditional_random_variables
    ):  # (random_variables~{random variable~d_distribution.RandomVariable}, conditional_random_variables~{random variable~d_distribution.RandomVariable})~dd_base.ProbabilityDistribution
        (random_variable,) = random_variables

        random_variables = self.get_random_variables()
        assert random_variable in random_variables

        self_conditional_random_variables = self.get_conditional_random_variables()

        if conditional_random_variables == self_conditional_random_variables:
            result = self.marginal_distributions[random_variable]
            return result

        (other_random_variable,) = d_python_tools.get_others(
            random_variable, random_variables
        )

        conditional_random_variables = set(conditional_random_variables)
        conditional_random_variables.remove(other_random_variable)

        if conditional_random_variables == self_conditional_random_variables:
            result = self._conditional_marginal_distributions.get(random_variable, None)
            if result is not None:
                return result

            result = _VineMarginalDistribution(self, other_random_variable)
            self._conditional_marginal_distributions[random_variable] = result
            return result

        raise NotImplementedError(type(self))

    def get_copula(
        self,
    ):  # ()~(copula distribution~ddc_base.CopulaDistribution, variable map~{random variable~d_distribution.RandomVariable: copula variable~d_distribution.RandomVariable})
        result = (self.copula_distribution, self.copula_variable_map)
        return result


class _VineMarginalDistribution(dd_base.ProbabilityDistribution):
    def __init__(
        self, vine_pair_distribution, random_variable
    ):  # (vine_pair_distribution~_VinePairDistribution, random_variable~d_distribution.RandomVariable)~_VineMarginalDistribution
        super().__init__()

        self.vine_pair_distribution = vine_pair_distribution
        self.random_variable = random_variable

    # probabilities

    def get_cumulative_probabilities(
        self, points
    ):  # (points~POINTS)~[cumulative probability~object]
        result = self._get_cumulative_probabilities(None, points)
        return result

    def _get_cumulative_probabilities(self, specialization_id, points):
        conditional_random_variables = (
            self.vine_pair_distribution.get_conditional_random_variables()
        )
        copula_distribution, variable_map = self.vine_pair_distribution.get_copula()

        marginal_distributions = (
            (
                variable_map[random_variable],
                self.vine_pair_distribution.get_marginal_distribution(
                    {random_variable}, conditional_random_variables
                ).get_specialization(specialization_id),
            )
            for random_variable in self.vine_pair_distribution.get_random_variables()
        )
        points = points & d_space.Points(
            {
                copula_random_variable: marginal_distribution.get_cumulative_probabilities(
                    points
                )
                for copula_random_variable, marginal_distribution in marginal_distributions
            },
            length=len(points),
        )

        result = copula_distribution.get_specialization(
            specialization_id
        ).get_cumulative_probability_gradients(
            points, variable_map[self.random_variable]
        )
        return result

    def get_quantiles(
        self, cumulative_probabilities, points
    ):  # (cumulative_probabilities~[cumulative probability~object], points~POINTS)~POINTS
        result = self._get_quantiles(None, cumulative_probabilities, points)
        return result

    def _get_quantiles(self, specialization_id, cumulative_probabilities, points):
        (other_random_variable,) = d_python_tools.get_others(
            self.random_variable, self.vine_pair_distribution.get_random_variables()
        )
        conditional_random_variables = (
            self.vine_pair_distribution.get_conditional_random_variables()
        )
        copula_distribution, variable_map = self.vine_pair_distribution.get_copula()

        points = points & d_space.Points(
            {
                variable_map[
                    self.random_variable
                ]: self.vine_pair_distribution.get_marginal_distribution(
                    {self.random_variable}, conditional_random_variables
                )
                .get_specialization(specialization_id)
                .get_cumulative_probabilities(points)
            },
            length=len(points),
        )
        (cumulative_probabilities,) = (
            copula_distribution.get_specialization(specialization_id)
            .get_cumulative_probability_gradient_inverses(
                cumulative_probabilities,
                points,
                variable_map[self.random_variable],
            )
            .get_coordinates()
        )
        result = (
            self.vine_pair_distribution.get_marginal_distribution(
                {other_random_variable}, conditional_random_variables
            )
            .get_specialization(specialization_id)
            .get_quantiles(cumulative_probabilities, points)
        )
        return result
