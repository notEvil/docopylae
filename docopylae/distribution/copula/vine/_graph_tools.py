import docopylae.python_tools as d_python_tools
import itertools


def get_node_matrix(
    id_matrix,
):  # (id_matrix~[column~[id~hash(object)]])~[column~[node~PairNode or MarginNode]]
    node_cache = {}
    result = []

    for column_index, column in enumerate(id_matrix):
        first_id = column[column_index]

        result_column = []

        for row_index, second_id in enumerate(column[:column_index]):
            ids = {first_id, second_id}
            conditional_ids = set(column[:row_index])

            pair_node = _get_pair_node(ids, conditional_ids, id_matrix, node_cache)
            assert pair_node is not None

            result_column.append(pair_node)

        margin_node = _get_margin_node(first_id, set(), id_matrix, node_cache)
        result_column.append(margin_node)

        result.append(result_column)

    return result


def _get_pair_node(
    ids, conditional_ids, id_matrix, cache
):  # (ids~{id~hash(object)}, conditional_ids~{id~hash(object)}, id_matrix~[column~[id~hash(object)]], cache~{(ids~hash({id~hash(object)}), conditional ids~hash({id~hash(object)})): PairNode})~PairNode
    cache_key = (frozenset(ids), frozenset(conditional_ids))

    result = cache.get(cache_key, d_python_tools.NONE)
    if result is not d_python_tools.NONE:
        return result

    margin_nodes = {}

    for id in ids:
        margin_node = _get_margin_node(id, conditional_ids, id_matrix, cache)

        if margin_node is None:
            result = None
            break

        margin_nodes[id] = margin_node

    else:
        result = PairNode(ids, conditional_ids, margin_nodes)

    cache[cache_key] = result
    return result


def _get_margin_node(
    id, conditional_ids, id_matrix, cache
):  # (id~hash(object), conditional_ids~{id~hash(object)}, id_matrix~[column~[id~hash(object)]], cache~{(id~hash(object), conditional ids~hash({id~hash(object)})): MarginNode})~MarginNode
    cache_key = (id, frozenset(conditional_ids))

    result = cache.get(cache_key, d_python_tools.NONE)
    if result is not d_python_tools.NONE:
        return result

    if len(conditional_ids) == 0:
        result = MarginNode(id, conditional_ids, None)
        cache[cache_key] = result
        return result

    for column_index, column in zip(
        itertools.count(len(conditional_ids)),
        id_matrix[len(conditional_ids) :],
    ):
        column_id = column[column_index]
        conditional_id = column[len(conditional_ids) - 1]

        if column_id == id:
            candidate_c_ids = set(column[: len(conditional_ids)])

        elif conditional_id == id:
            candidate_c_ids = set(column[: (len(conditional_ids) - 1)])
            candidate_c_ids.add(column_id)

        else:
            continue

        if candidate_c_ids != conditional_ids:
            continue

        sub_conditional_ids = conditional_ids.copy()
        sub_conditional_ids.remove(conditional_id if column_id == id else column_id)

        pair_node = _get_pair_node(
            {column_id, conditional_id}, sub_conditional_ids, id_matrix, cache
        )
        if pair_node is None:
            continue

        result = MarginNode(id, conditional_ids, pair_node)
        break

    else:
        result = None

    cache[cache_key] = result
    return result


class PairNode:
    def __init__(
        self, ids, conditional_ids, margin_nodes
    ):  # (ids~{id~hash(object)}, conditional_ids~{id~hash(object)}, margin_nodes~{id~hash(object): margin node~MarginNode})~PairNode
        self.ids = ids
        self.conditional_ids = conditional_ids
        self.margin_nodes = margin_nodes

    def __repr__(self):
        result = "PairNode(ids={}, conditional_ids={}, margin_nodes={})".format(
            repr(self.ids), repr(self.conditional_ids), repr(self.margin_nodes)
        )
        return result


class MarginNode:
    def __init__(
        self, id, conditional_ids, pair_node
    ):  # (id~hash(object), conditional_ids~{id~hash(object)}, pair_node~PairNode)~MarginNode
        self.id = id
        self.conditional_ids = conditional_ids
        self.pair_node = pair_node

    def __repr__(self):
        result = "MarginNode(id={}, conditional_ids={}, pair_node={})".format(
            repr(self.id), repr(self.conditional_ids), repr(self.pair_node)
        )
        return result


def get_dot_lines(
    node_matrix, prefix=None
):  # (node_matrix~[column~[node~PairNode or MarginNode]], prefix~str or None)~iter(line~str)
    if prefix is None:
        prefix = "_"

    cache = {}

    for column_index, column in enumerate(node_matrix):
        for pair_node in column[:column_index]:
            yield from _visit_pair_node(pair_node, prefix, cache)


def _visit_pair_node(pair_node, prefix, cache):
    cache_key = pair_node

    result = cache.get(cache_key, None)
    if result is not None:
        return result

    node_id = "{}{}".format(prefix, id(pair_node))
    cache[cache_key] = node_id

    node_label = _get_label(pair_node.get_ids(), pair_node.conditional_ids)
    yield '{} [ label="{}" ]'.format(node_id, node_label)

    for margin_node in pair_node.get_margin_nodes().values():
        target_node_id = yield from _visit_margin_node(margin_node, prefix, cache)
        yield "{} -> {}".format(node_id, target_node_id)

    return node_id


def _visit_margin_node(margin_node, prefix, cache):
    cache_key = margin_node

    result = cache.get(cache_key, None)
    if result is not None:
        return result

    node_id = "{}{}".format(prefix, id(margin_node))
    cache[cache_key] = node_id

    node_label = _get_label([margin_node.get_id()], margin_node.conditional_ids)
    yield '{} [ label="{}" ]'.format(node_id, node_label)

    pair_node = margin_node.get_pair_node()
    if pair_node is not None:
        target_node_id = yield from _visit_pair_node(pair_node, prefix, cache)
        yield "{} -> {}".format(node_id, target_node_id)

    return node_id


def _get_label(ids, conditional_ids):
    result = "{}{}".format(
        ", ".join(map(str, ids)),
        ""
        if len(conditional_ids) == 0
        else " | {}".format(", ".join(map(str, conditional_ids))),
    )
    return result
