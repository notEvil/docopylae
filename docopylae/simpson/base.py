class ApproximateIntegral:
    def evaluate(
        self, begin_argument, end_argument
    ):  # (begin_argument~object or None, end_argument~object)~object
        raise NotImplementedError(type(self))

    def evaluate_many(
        self, begin_arguments, end_arguments
    ):  # (begin_arguments~numpy.ndarray([begin argument~object]) or None, end_arguments~numpy.ndarray([end argument~object]))~numpy.ndarray([area~object])
        raise NotImplementedError(type(self))

    def evaluate_inverse(self, area):  # (area~object)~object
        raise NotImplementedError(type(self))

    def evaluate_inverses(
        self, areas
    ):  # (areas~numpy.ndarray([area~object]))~numpy.ndarray([end argument~object])
        raise NotImplementedError(type(self))
