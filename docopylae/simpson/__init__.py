"""
- provides classes which efficiently approximate arbitrary 1-dimensional definite integrals and inverses
- currently uses Adaptive Simpson's method
"""

import docopylae.simpson.base as ds_base
import numpy
import pandas


def _get_adaptive_simpson_intervals(
    begin_argument, end_argument, function, epsilon=1e-9, max_evaluations=int(1e3)
):
    """
    - based on https://en.wikipedia.org/wiki/Adaptive_Simpson%27s_method#Python
    """

    n_evaluations = 5  # 3 init + 2 first interval

    if max_evaluations < n_evaluations:
        return

    begin_value = function(begin_argument)
    end_value = function(end_argument)
    middle_argument, middle_value, whole = _evaluate_simpsons_rule(
        begin_argument, begin_value, end_argument, end_value, function
    )
    interval = (
        None,  # sort key
        begin_argument,
        begin_value,
        middle_argument,
        middle_value,
        end_argument,
        end_value,
        whole,
        None,  # delta
        epsilon * 15,
    )
    intervals = [interval]

    while True:
        intervals.sort()

        (
            _,
            begin_argument,
            begin_value,
            middle_argument,
            middle_value,
            end_argument,
            end_value,
            whole,
            _,
            max_delta,
        ) = intervals.pop()

        middle_argument_1, middle_value_1, whole_1 = _evaluate_simpsons_rule(
            begin_argument, begin_value, middle_argument, middle_value, function
        )
        middle_argument_2, middle_value_2, whole_2 = _evaluate_simpsons_rule(
            middle_argument, middle_value, end_argument, end_value, function
        )
        delta = whole_1 + whole_2 - whole
        abs_delta = abs(delta)

        if abs_delta <= max_delta:
            delta_30 = delta / 30  # delta / 15 / 2
            interval = (
                begin_argument,
                begin_value,
                middle_argument_1,
                middle_value_1,
                middle_argument,
                middle_value,
                whole_1 + delta_30,  # TODO fine?
            )
            yield interval
            interval = (
                middle_argument,
                middle_value,
                middle_argument_2,
                middle_value_2,
                end_argument,
                end_value,
                whole_2 + delta_30,  # TODO fine?
            )
            yield interval

            if len(intervals) == 0:
                break

        else:
            max_delta_2 = max_delta / 2
            intervals.append(
                (
                    abs_delta,  # TODO correct?
                    begin_argument,
                    begin_value,
                    middle_argument_1,
                    middle_value_1,
                    middle_argument,
                    middle_value,
                    whole_1,
                    delta,
                    max_delta_2,
                )
            )
            intervals.append(
                (
                    abs_delta,  # TODO correct?
                    middle_argument,
                    middle_value,
                    middle_argument_2,
                    middle_value_2,
                    end_argument,
                    end_value,
                    whole_2,
                    delta,
                    max_delta_2,
                )
            )

        n_evaluations += 2
        if max_evaluations < n_evaluations:
            break

    for (
        _,
        begin_argument,
        begin_value,
        middle_argument,
        middle_value,
        end_argument,
        end_value,
        whole,
        delta,
        _,
    ) in intervals:
        interval = (
            begin_argument,
            begin_value,
            middle_argument,
            middle_value,
            end_argument,
            end_value,
            whole + delta / 30,
        )
        yield interval


def _evaluate_simpsons_rule(
    begin_argument, begin_value, end_argument, end_value, function
):
    middle_argument = (begin_argument + end_argument) / 2
    middle_value = function(middle_argument)
    whole = (
        abs(end_argument - begin_argument)
        / 6
        * (begin_value + 4 * middle_value + end_value)
    )
    result = (middle_argument, middle_value, whole)
    return result


def _get_quadratic_polynomial_coefficients(
    begin_argument, begin_value, middle_argument, middle_value, end_argument, end_value
):
    """
    - derived with Mathematica, cached with R.Deriv::Cache
    """

    x1, y1, x2, y2, x3, y3 = (
        begin_argument,
        begin_value,
        middle_argument,
        middle_value,
        end_argument,
        end_value,
    )

    _e1 = x1 - x2
    _e2 = x2 - x3
    _e3 = _e1 * (x1 - x3) * _e2
    _e4 = -y1
    _e5 = -x1
    _e6 = x3 ** 2
    _e17 = (x2 * _e2 * x3 * y1 + x1 * x3 * (_e5 + x3) * y2 + x1 * _e1 * x2 * y3) / _e3
    _e19 = (x3 * (_e4 + y2) + x2 * (y1 - y3) + x1 * (-y2 + y3)) / _e3
    _e21 = (_e6 * (y1 - y2) + x1 ** 2 * (y2 - y3) + x2 ** 2 * (_e4 + y3)) / _e3
    result = (_e19, _e21, _e17)
    return result


def _get_quadratic_polynomial_area(begin_argument, end_argument, coefficients):
    """
    - derived with Mathematica, cached with R.Deriv::Cache
    """

    x1, x2, (a, b, c) = begin_argument, end_argument, coefficients

    _e1 = 2 * a
    _e2 = 3 * b
    _e3 = 6 * c
    result = (
        1
        / 6
        * (
            -x1 * (_e3 + x1 * (_e2 + _e1 * x1))
            + _e3 * x2
            + _e2 * x2 ** 2
            + _e1 * x2 ** 3
        )
    )
    return result


def _get_quadratic_polynomial_argument(
    area, begin_argument, end_argument, coefficients
):
    """
    - derived with Mathematica
    """

    x1, (a, b, c) = begin_argument, coefficients

    coefficient_3 = 2 * a
    coefficient_2 = 3 * b
    coefficient_1 = 6 * c
    coefficients = [
        coefficient_3,
        coefficient_2,
        coefficient_1,
        -6 * area
        - coefficient_1 * x1
        - coefficient_2 * x1 ** 2
        - coefficient_3 * x1 ** 3,
    ]
    roots = numpy.roots(coefficients)

    for root in roots:
        if not (
            root.imag == 0 and begin_argument <= root.real and root.real <= end_argument
        ):
            continue

        return root

    result = (begin_argument + end_argument) / 2  # TODO find a better solution
    return result


class ApproximateIntegral(ds_base.ApproximateIntegral):
    """
    - uses Adaptive Simpson's method to approximate any 1-dimensional definite integral or inverse
    """

    def __init__(
        self,
        begin_argument,
        end_argument,
        function,
        epsilon=1e-9,
        max_evaluations=int(1e3),
    ):
        super().__init__()

        self.begin_argument = begin_argument
        self.end_argument = end_argument
        self.function = function
        self.epsilon = epsilon
        self.max_evaluations = max_evaluations

        self._intervals = None

    def _initialize(self):
        if self._intervals is not None:
            return

        intervals = _get_adaptive_simpson_intervals(
            self.begin_argument,
            self.end_argument,
            self.function,
            epsilon=self.epsilon,
            max_evaluations=self.max_evaluations,
        )
        intervals = pandas.DataFrame(
            intervals,
            columns=[
                "begin_argument",
                "begin_value",
                "middle_argument",
                "middle_value",
                "end_argument",
                "end_value",
                "area",
            ],
        )
        intervals.sort_values("begin_argument", inplace=True)
        intervals["area"] = intervals["area"].cumsum().shift(fill_value=0.0)

        intervals = intervals.assign(
            **dict(
                zip(
                    ["c2", "c1", "c0"],
                    _get_quadratic_polynomial_coefficients(
                        intervals["begin_argument"].values,
                        intervals["begin_value"].values,
                        intervals["middle_argument"].values,
                        intervals["middle_value"].values,
                        intervals["end_argument"].values,
                        intervals["end_value"].values,
                    ),
                )
            )
        )

        self._intervals = intervals

    def evaluate(
        self, begin_argument, end_argument
    ):  # (begin_argument~object or None, end_argument~object)~object
        self._initialize()

        result = self._evaluate(end_argument)
        if begin_argument is not None:
            result -= self._evaluate(begin_argument)

        return result

    def _evaluate(self, argument):
        index = self._intervals["end_argument"].searchsorted(argument)

        if index == len(self._intervals.index):
            result = self._intervals["area"].iloc[-1]
            return result

        interval = self._intervals.iloc[index, :]

        if index == 0 and argument <= interval["begin_argument"]:
            return 0.0

        result = interval["area"] + _get_quadratic_polynomial_area(
            interval["begin_argument"], argument, interval[["c2", "c1", "c0"]]
        )
        return result

    def evaluate_many(
        self, begin_arguments, end_arguments
    ):  # (begin_arguments~numpy.ndarray([begin argument~object]) or None, end_arguments~numpy.ndarray([end argument~object]))~numpy.ndarray([area~object])
        self._initialize()

        result = self._evaluate_many(end_arguments)
        if begin_arguments is not None:
            result -= self._evaluate_many(begin_arguments)

        return result

    def _evaluate_many(self, arguments):
        indices = self._intervals["end_argument"].searchsorted(arguments)

        (end_indices,) = numpy.nonzero(indices == len(self._intervals.index))
        indices[end_indices] = -1

        intervals = self._intervals.iloc[indices, :]

        result = intervals["area"].values + _get_quadratic_polynomial_area(
            intervals["begin_argument"].values,
            arguments,
            (intervals["c2"].values, intervals["c1"].values, intervals["c0"].values),
        )
        result[(indices == 0) & (arguments <= intervals["begin_argument"].values)] = 0.0
        result[end_indices] = self._intervals["area"].iloc[-1]
        return result

    def evaluate_inverse(self, area):  # (area~object)~object
        self._initialize()

        index = self._intervals["area"].searchsorted(area)

        if index == 0:
            return self.begin_argument

        if (
            index == len(self._intervals.index)
            and self._intervals["area"].iloc[-1] <= area
        ):
            return self.end_argument

        interval = self._intervals.iloc[index - 1, :]

        result = _get_quadratic_polynomial_argument(
            area - interval["area"],
            interval["begin_argument"],
            interval["end_argument"],
            interval[["c2", "c1", "c0"]],
        )
        return result

    def evaluate_inverses(
        self, areas
    ):  # (areas~numpy.ndarray([area~object]))~numpy.ndarray([end argument~object])
        self._initialize()

        indices = self._intervals["area"].searchsorted(areas)
        intervals = self._intervals.iloc[indices - 1, :]

        result = numpy.array(
            [
                _get_quadratic_polynomial_argument(
                    area, begin_argument, end_argument, (c2, c1, c0)
                )
                for area, begin_argument, end_argument, c2, c1, c0 in zip(
                    areas - intervals["area"].values,
                    intervals["begin_argument"].values,
                    intervals["end_argument"].values,
                    intervals["c2"].values,
                    intervals["c1"].values,
                    intervals["c0"].values,
                )
            ]
        )
        result[indices == 0] = self.begin_argument
        result[
            (indices == len(self._intervals.index))
            & (self._intervals["area"].iloc[-1] <= areas)
        ] = self.end_argument
        return result


class InfiniteApproximateIntegral(ds_base.ApproximateIntegral):
    def __init__(
        self,
        begin_argument,
        center_begin_argument,
        center_end_argument,
        end_argument,
        function,
        epsilon=1e-9,
        max_evaluations=int(1e3),
    ):
        super().__init__()

        self.begin_argument = begin_argument
        self.center_begin_argument = center_begin_argument
        self.center_end_argument = center_end_argument
        self.end_argument = end_argument
        self.function = function
        self.epsilon = epsilon
        self.max_evaluations = max_evaluations

        self._location = (center_begin_argument + center_end_argument) / 2
        self._scale = (
            center_end_argument - center_begin_argument
        ) / 6  # 6 === derivative of transform at center_begin_argument and center_end_argument is 0.1

        self._approximate_integral = ApproximateIntegral(
            self._transform(begin_argument),
            self._transform(end_argument),
            self._function,
        )

    def _transform(self, argument):
        (result,) = self._transform_many(numpy.array([argument]))
        return result

    def _invert(self, finite_argument, with_jacobian):
        ((argument,), jacobian) = self._invert_many(
            numpy.array([finite_argument]), with_jacobian
        )
        if with_jacobian:
            (jacobian,) = jacobian
        result = (argument, jacobian)
        return result

    def _transform_many(self, arguments):
        result = numpy.arctan((arguments - self._location) / self._scale)
        return result

    def _invert_many(self, finite_arguments, with_jacobian):
        arguments = numpy.tan(finite_arguments) * self._scale + self._location

        if with_jacobian:
            jacobians = self._scale / numpy.cos(finite_arguments) ** 2

        else:
            jacobians = None

        result = (arguments, jacobians)
        return result

    def _function(self, finite_argument, _bound=numpy.pi / 2):
        if abs(finite_argument) == _bound:
            return 0.0
        argument, jacobian = self._invert(finite_argument, True)
        result = self.function(argument) * jacobian
        return result

    def evaluate(
        self, begin_argument, end_argument
    ):  # (begin_argument~object or None, end_argument~object)~object
        result = self._approximate_integral.evaluate(
            None if begin_argument is None else self._transform(begin_argument),
            self._transform(end_argument),
        )
        return result

    def evaluate_many(
        self, begin_arguments, end_arguments
    ):  # (begin_arguments~numpy.ndarray([begin argument~object]) or None, end_arguments~numpy.ndarray([end argument~object]))~numpy.ndarray([area~object])
        result = self._approximate_integral.evaluate_many(
            None if begin_arguments is None else self._transform_many(begin_arguments),
            self._transform_many(end_arguments),
        )
        return result

    def evaluate_inverse(self, area):  # (area~object)~object
        result, _ = self._invert(
            self._approximate_integral.evaluate_inverse(area), False
        )
        return result

    def evaluate_inverses(
        self, areas
    ):  # (areas~numpy.ndarray([area~object]))~numpy.ndarray([end argument~object])
        result, _ = self._invert_many(
            self._approximate_integral.evaluate_inverses(areas), False
        )
        return result
