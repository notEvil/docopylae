# _docopylae_

is a reimplementation of [_docopulae_](https://cran.r-project.org/package=docopulae) in _Python_. Its main purpose is to provide a flexible framework for multivariate statistical models and methods from Optimal Design of Experiments.

### Motivation

_docopulae_ is a flexible framework written in _R_ and has been stable and battle-tested for a long time now. However, it inherits many disadvantages from _R_ and doesn't integrate well with new developments in computational methods, most prominently used for AI. _R_ is a specialized language for user-focused numeric programming and for this reason _docopulae_ often has to stretch beyond the safe boundaries set by _R_. As a consequence, it is rather difficult to correctly use _docopulae_, in many cases leading to wrong results.

_Python_ on the other hand is an increasingly popular general-purpose programming language that provides a simple yet powerful type system. It is well suited for abstract programming required by frameworks like _docopulae_. _Python_ itself integrates well with other technologies, including _R_, thus providing many opportunities to pick the best tool for the particular job.

### Differences to _docopulae_

- _docopylae_ is object-oriented (simple inheritance, interfaces, dependency-injection, serialization, ...) instead of functional
- _docopylae_ is easier to use correctly
- _docopylae_ is more flexible/hackable and more powerful
- _docopylae_ is not complete and less stable as of now, see TODO

### TODO
- rename docopulae to docopylae
- provide examples
- publish on pypi
- add quickstart
- use mypy
- write documentation
- add more copulas
- improve numerical integration
- consider replacing functions by objects
